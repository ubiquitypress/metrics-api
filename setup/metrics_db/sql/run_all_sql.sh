#!/bin/bash

# Directory containing the SQL files
SQL_DIR="setup/metrics_db/sql"

# Database credentials from environment variables
DB_USER="${POSTGRES_USER:-postgres}"
DB_NAME="${POSTGRES_DB:-postgres}"
DB_HOST="${POSTGRES_HOST:-localhost}"
DB_PASSWORD="${POSTGRES_PASSWORD:-postgres}"

# Export the password for `psql`
export PGPASSWORD="$DB_PASSWORD"

# Change to the SQL directory
cd "$SQL_DIR" || exit

# Loop through each .sql file in the directory, sorted numerically
for sql_file in $(ls *.sql | sort -V); do
  echo "Executing $sql_file..."
  psql -U "$DB_USER" -d "$DB_NAME" -h "$DB_HOST" -f "$sql_file"
  if [ $? -ne 0 ]; then
    echo "Error executing $sql_file"
    exit 1
  fi
done

echo "All SQL scripts executed successfully."
