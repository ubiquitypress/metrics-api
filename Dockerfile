FROM python:3.10

ENV APP_USER=operas
ENV APP_HOME=/usr/src/app
WORKDIR $APP_HOME

COPY ./config/requirements.txt ./
COPY .flake8 ./

RUN apt-get update && apt-get -y install apt-utils supervisor \
    && pip install --no-cache-dir -r requirements.txt \
    && rm requirements.txt

RUN addgroup --gid 987 $APP_USER \
    && adduser -uid 987 --system --group $APP_USER \
    && chown -R $APP_USER:$APP_USER $APP_HOME

USER $APP_USER
COPY ./config/supervisord*.conf /etc/supervisor/
ADD ./src/ ./

EXPOSE 8080
ENTRYPOINT  ["/usr/bin/supervisord", "-c", "/etc/supervisor/supervisord.conf"]
