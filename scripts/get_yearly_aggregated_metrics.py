import os
import pandas as pd
from sqlalchemy import create_engine, text
import itertools
import threading
import time
import sys

# Loading animation flag
done = False


def connect_to_taskmaster_db():
    """Connects to the Taskmaster database using environment variables."""
    username = os.getenv("TASKMASTER_DB_USER", "postgres")
    password = os.getenv("TASKMASTER_DB_PASSWORD", "")
    host = os.getenv("TASKMASTER_DB_HOST", "localhost")
    port = os.getenv("TASKMASTER_DB_PORT", "5432")
    database = os.getenv("TASKMASTER_DB_NAME", "taskmaster")
    db_url = f'postgresql://{username}:{password}@{host}:{port}/{database}'
    return create_engine(db_url)


def connect_to_metrics_api_db():
    """Connects to the Metrics API database using environment variables."""
    username = os.getenv("METRICS_API_DB_USER", "postgres")
    password = os.getenv("METRICS_API_DB_PASSWORD", "")
    host = os.getenv("METRICS_API_DB_HOST", "localhost")
    port = os.getenv("METRICS_API_DB_PORT", "54322")
    database = os.getenv("METRICS_API_DB_NAME", "metrics-api")
    db_url = f'postgresql://{username}:{password}@{host}:{port}/{database}'
    return create_engine(db_url)


def animate_loading():
    """Displays a loading animation in the console.
    Separate in batches so the result is easier to read."""
    for c in itertools.cycle(['|', '/', '-', '\\']):
        if done:
            break
        sys.stdout.write('\rLoading ' + c)
        sys.stdout.flush()
        time.sleep(0.1)
    sys.stdout.write('\rDone!     \n')


def generate_uuids_and_title_from_tm(engine):
    """Generates UUID and title data from the Taskmaster database.
    Change the tenant at your needs:
    IE: University of North Alabama -> 'dashboard.ir.una.edu'"""
    tenant = os.getenv('TENANT', 'dashboard.ir.una.edu')
    query = f"""
        SELECT work.uuid,
        TRIM('" ' FROM (jsonb_array_elements(work.metadata -> 'title'))::text) AS title
        FROM processor_repositorywork AS work
        INNER JOIN processor_repositorytenant AS tenant
        ON work.tenant_id = tenant.uuid
        WHERE tenant.cname = '{tenant}'
        AND work.is_deleted = false
    """
    with engine.connect() as connection:
        result = connection.execute(text(query))
        titles_data = pd.DataFrame(result.fetchall(), columns=result.keys())

    # Convert to formats
    uuid_list = titles_data['uuid'].astype(str).tolist()
    titles_data['title'] = titles_data['title'].astype(str)
    titles_data['uuid'] = titles_data['uuid'].astype(str)

    return titles_data, uuid_list


def fetch_event_data_in_batches(uuid_list, engine, batch_size=100):
    """Fetches event data from the database in batches."""
    all_event_data = []
    num_batches = (len(uuid_list) + batch_size - 1) // batch_size

    for i in range(num_batches):
        batch_uuids = uuid_list[i * batch_size:(i + 1) * batch_size]
        placeholders = ', '.join([f"'{uuid}'" for uuid in batch_uuids])
        query = f"""
        SELECT * FROM event
        WHERE REPLACE(work_uri, 'urn:uuid:', '') IN ({placeholders})
        """
        print(f"Querying database for batch {i + 1}/{num_batches}...")
        with engine.connect() as connection:
            result = connection.execute(text(query))
            batch_data = pd.DataFrame(result.fetchall(), columns=result.keys())
            all_event_data.append(batch_data)

    return pd.concat(all_event_data, ignore_index=True)


def process_event_data(event_data):
    """Processes and filters event data."""
    event_data['timestamp'] = pd.to_datetime(
        event_data['timestamp'], utc=True, errors='raise'
    )
    print("Converted timestamps to UTC.")

    start_date = os.getenv("START_DATE", '2021-08-01')

    event_data = event_data[event_data['timestamp'] >= start_date]
    print(f"Filtered data to entries from {start_date} onwards.")

    event_data['year'] = event_data['timestamp'].dt.year
    print("Extracted year from timestamp.")

    event_data['type'] = event_data['measure_uri'].apply(
        lambda x: 'views' if 'sessions' in x else 'downloads'
        if 'downloads' in x else None
    )
    event_data = event_data[event_data['type'].notnull()]
    print("Categorized entries as views or downloads.")

    event_data['work_uri'] = event_data['work_uri'].str.replace(
        'urn:uuid:', '', regex=False
    )
    print("Cleaned work_uri for matching.")

    return event_data


def merge_and_aggregate_data(event_data, titles_data):
    """Merges event data with title data and aggregates 'value' by summing
    entries for repeated titles, types, years, and uploader URIs.
    Restructures the DataFrame so each title has a single row with columns
    for each year's views and downloads.
    """
    merged_data = event_data.merge(
        titles_data, left_on='work_uri', right_on='uuid', how='left'
    )
    merged_data['title'] = merged_data['title'].fillna(merged_data['work_uri'])
    merged_data.drop(columns=['uuid'], inplace=True, errors='ignore')
    merged_data['value'] = merged_data['value'].astype(int)

    aggregated_data = merged_data.groupby(
        ['title', 'year', 'type', 'uploader_uri'], as_index=False
    )['value'].sum()

    pivot_data = aggregated_data.pivot_table(
        index=['title', 'uploader_uri'],
        columns=['year', 'type'],
        values='value',
        fill_value=0
    )

    pivot_data.columns = [f"{year} {typ}" for year, typ in pivot_data.columns]
    pivot_data.reset_index(inplace=True)
    pivot_data = pivot_data.astype(int, errors='ignore')

    print("Final DataFrame after pivoting:")
    print(pivot_data)

    return pivot_data


def save_data_to_csv(data, file_path):
    """Saves the processed data to a CSV file."""
    data.to_csv(file_path, index=False)
    print(f"Results saved to: {file_path}")


def main():
    """Main function to orchestrate the data loading and processing."""
    global done
    t = threading.Thread(target=animate_loading)
    taskmaster_engine = connect_to_taskmaster_db()
    metrics_api_engine = connect_to_metrics_api_db()
    t.start()

    try:
        titles_data, uuid_list = generate_uuids_and_title_from_tm(
            taskmaster_engine
        )
        event_data = fetch_event_data_in_batches(uuid_list, metrics_api_engine)
        done = True
        t.join()

        event_data = process_event_data(event_data)
        time.sleep(5)

        annual_breakdown = merge_and_aggregate_data(event_data, titles_data)
        output_file = 'annual_item_breakdown.csv'
        save_data_to_csv(annual_breakdown, output_file)

    except Exception as e:
        done = True
        t.join()
        print(f"An error occurred during processing: {e}")


if __name__ == "__main__":
    main()
