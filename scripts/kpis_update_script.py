import csv
import itertools
import json
import os
import sys
import time
import traceback
from calendar import monthrange
from concurrent.futures import ThreadPoolExecutor
from datetime import datetime, timedelta
from typing import Any, Dict, List, Optional

import pandas as pd
import requests
import threading
from sqlalchemy import create_engine, text


not_found_uris = set()
lock = threading.Lock()  # To ensure thread-safe access to the set


def connect_to_metrics_api_db():
    """Connects to the Metrics API database using environment variables."""
    username = os.getenv('METRICS_API_DB_USER', 'postgres')
    password = os.getenv('METRICS_API_DB_PASSWORD', '')
    host = os.getenv('METRICS_API_DB_HOST', 'localhost')
    port = os.getenv('METRICS_API_DB_PORT', '54322')
    database = os.getenv('METRICS_API_DB_NAME', 'metrics-api')
    db_url = f'postgresql://{username}:{password}@{host}:{port}/{database}'
    return create_engine(db_url)


def connect_to_identifiers_db():
    """Connects to the identifiers database using environment variables."""
    username = os.getenv('IDENTIFIERS_DB_USER', 'postgres')
    password = os.getenv('IDENTIFIERS_DB_PASSWORD', '')
    host = os.getenv('IDENTIFIERS_DB_HOST', 'localhost')
    port = os.getenv('IDENTIFIERS_DB_PORT', '54323')
    database = os.getenv('IDENTIFIERS_DB_NAME', 'identifiers-api')
    db_url = f'postgresql://{username}:{password}@{host}:{port}/{database}'
    return create_engine(db_url)

def execute_metrics_api_query(
    end_date: datetime,
    sql_filter: str,
) -> (pd.DataFrame, pd.DataFrame):
    """
    Execute queries to fetch data from the metrics API.
    The approach is collect the cumulative results.

    Returns:
        metrics_results: DataFrame containing matched `work_uri` values.
    """
    # Fetch work_uris from metrics API database
    engine_metrics_api = connect_to_metrics_api_db()
    print('execute_metrics_api_query')
    if not sql_filter:
        sql_filter = ''

    metrics_query = f'''
    SELECT
      work_uri, 
      timestamp, 
      SUBSTRING(work_uri FROM '^([^/]+)') AS truncated_work_uri
    FROM
      event
    WHERE
      work_uri LIKE 'info:doi:10%'
      {sql_filter}
      AND timestamp < '{end_date} 00:00:00';
    '''

    # Execute query and fetch results
    with engine_metrics_api.connect() as connection:
        results = connection.execute(text(metrics_query)).fetchall()
        metrics_results = pd.DataFrame(
            results,
            columns=['work_uri', 'timestamp', 'truncated_work_uri']
        )
    return metrics_results

def execute_identifiers_api_query_and_merge(
    metrics_api_results: pd
):
    print('execute_identifiers_api_query_and_merge')
    engine_identifiers = connect_to_identifiers_db()

    identifiers_query = '''
        SELECT
            CONCAT(wu.uri_scheme, ':', wu.uri_value) AS uri_values,
            wt.work_type
        FROM
            work_uri AS wu
        JOIN
            work AS wt ON wu.work_id = wt.work_id
        WHERE
            work_type IN ('monograph', 'book', 'edited-book');
    '''
    with engine_identifiers.connect() as connection:
        results = connection.execute(text(identifiers_query)).fetchall()
        identifiers_results = pd.DataFrame(
            results,
            columns=['uri_values', 'work_type']
        )

    # Merge results to find matches
    identifiers_results.rename(columns={'uri_values': 'work_uri'}, inplace=True)
    merged_df = metrics_api_results.merge(
        identifiers_results, on='work_uri', how='left', indicator=True
    )

    # Keep the _merge column for filtering
    unique_entries = merged_df.drop_duplicates(subset=['work_uri'])

    # Separate matched and unmatched data
    matched_df = unique_entries[
        unique_entries['_merge'] == 'both'
    ].drop(columns=['_merge'])
    unmatched_df = unique_entries[
        unique_entries['_merge'] == 'left_only'
    ].drop(columns=['_merge'])

    return matched_df, unmatched_df

def execute_queries_monthly(start_date):
    """This method is used when "monthly" is passed as a parameter,
    otherwise will count cumulative numbers."""
    today = datetime.today().date()
    current_date = start_date

    sql_filter = f'''
    AND timestamp >= '{start_date} 00:00:00'
    '''

    while current_date <= today:
        print('CURRENT DATE?:', current_date)
        print('TODAY:', today)
        end_date_month = current_date.replace(
            day=monthrange(current_date.year, current_date.month)[1]
        )
        execute_queries_and_filter(end_date_month, sql_filter)
        current_date = end_date_month + timedelta(days=1)

def execute_queries_and_filter(
        end_date: datetime,
        sql_filter: str
) -> tuple[list, Any]:
    """Loop using the start_date always as a starting point until today's
    date, filter the distinct work_uri values for the current month and print
    them.
    """
    query_results = []
    count_dataset = {}

    end_date_month = end_date.replace(
        day=monthrange(end_date.year, end_date.month)[1]
    )

    df_metrics_api_results = execute_metrics_api_query(
        end_date_month,
        sql_filter
    )

    df_results, unmatched_books = execute_identifiers_api_query_and_merge(
        df_metrics_api_results
    )

    filtered_df, publisher_count, books_count = filter_and_count_ocurrences(
        df_results, end_date_month
    )
    count_dataset[f'PUBLISHERS: {end_date_month.strftime("%Y-%m")}'] = (
        publisher_count
    )
    count_dataset[f'BOOKS: {end_date_month.strftime("%Y-%m")}'] = (
        books_count
    )

    query_results.append(filtered_df)

    for dataset_month, count in count_dataset.items():
        print(f'Distinct work_uri count for {dataset_month}: {count}')

    return query_results, unmatched_books

def filter_and_count_ocurrences(
        dataset: pd.DataFrame,
        end_date_month: timedelta
):
    """Filter by timestamp, and count occurrences for publishers and books."""

    dataset['timestamp'] = pd.to_datetime(dataset['timestamp'], errors='coerce')
    filtered_df = dataset[dataset['timestamp'].dt.date <= end_date_month]

    truncated_work_uri_count = (
        filtered_df['truncated_work_uri'].nunique()
    )

    work_uri_count = (
        filtered_df['work_uri'].nunique()
    )

    return filtered_df, truncated_work_uri_count, work_uri_count

def fetch_work_uri_data_from_crossref(prefix):
    """Call crossref endpoint to translate the DOIs
    to the publishers names. If not found will try
    with DataCite instead.
    """
    url = (
        f'https://api.crossref.org/prefixes/{prefix}/works?'
        'filter=type:monograph,'
        'type:book,'
        'type:reference-book,'
        'type:edited-book&'
        'rows=1000'
    )
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()

def fetch_work_uri_data_from_datacite(truncated_work: str) -> Optional[Dict[str, Any]]:
    """
    Call the DataCite REST API to retrieve publishers. Log 404 errors to a set.

    Args:
        truncated_work (str): The truncated work URI (prefix).

    Returns:
        Optional[Dict[str, Any]]: Data from the API, or None if not found
        or an error occurred.
    """

    truncated_work_cleaned = truncated_work.replace('info:doi:', '')

    # Construct the API URL with the cleaned truncated_work
    url = f'https://api.datacite.org/dois?prefix={truncated_work_cleaned}'

    try:
        response = requests.get(url)
        response_data = response.json()

        if response.status_code != 200 or not response_data.get('data'):
            with lock:
                not_found_uris.add(truncated_work)

        return response_data

    except requests.exceptions.RequestException as e:
        print(f'Error fetching {truncated_work}: {e}')

def save_dois_to_json(matched_dois: list[dict], filename: str) -> None:
    """
    Saves the matched DOI metadata to a JSON file, creating any missing directories.

    Args:
        matched_dois (list[dict]): List of DOI metadata dictionaries.
        filename (str): Path of the JSON file to save the data.
    """
    # Create directories if they do not exist
    os.makedirs(os.path.dirname(filename), exist_ok=True)

    with open(filename, 'w', encoding='utf-8') as f:
        json.dump(matched_dois, f, indent=4, ensure_ascii=False)

    print(f'Saved {len(matched_dois)} records to {filename}')

def animate_loading(done, error_message):
    """Displays a loading animation in the console."""
    for c in itertools.cycle(['|', '/', '-', '\\']):
        if done.is_set():
            break
        sys.stdout.write('\rLoading ' + c)
        sys.stdout.flush()
        time.sleep(0.1)
    sys.stdout.write('\rDone!     \n')

    if error_message[0]:
        sys.stdout.write(f'\nError: {error_message[0]}\n')


class DataProcessor:
    @staticmethod
    def process_data(
            query_result: List[pd.DataFrame],
            not_found_books: pd.DataFrame,
    ) -> dict[str, list[dict[str, Any]]]:
        """
        Processes the data from query results, fetching publishers and prefixes
        based on truncated and full work URIs.

        Args:
            query_result (List[pd.DataFrame]): List of DataFrames with query results.
            not_found_books (pd.DataFrame): DataFrame containing books that were not found
            in the database.

        Returns:
            Dict[str, List[Dict[str, Any]]]: Processed results containing publisher and prefix data.
        """
        df_combined = pd.concat(query_result, ignore_index=True)

        unique_truncated_uris = df_combined['truncated_work_uri'].unique()
        print('Truncated LEN : ', len(unique_truncated_uris))

        books = df_combined.assign(publisher='')[['work_uri', 'work_type']]
        books.rename(columns={'work_uri': 'DOI', 'work_type': 'type'}, inplace=True)

        books = books.to_dict(orient='records')

        with ThreadPoolExecutor(max_workers=5) as executor:
            results = list(executor.map(
                lambda truncated_uri: DataProcessor.fetch_data_from_apis(
                    truncated_uri, not_found_books
                ),
                unique_truncated_uris
            ))

        unique_publishers = {}
        for result in results:
            for publisher in result['publishers']:
                key = (publisher['publisher'], publisher['prefix'])
                if key not in unique_publishers:
                    unique_publishers[key] = publisher

        api_books = [book for result in results for book in result['books']]

        combined_books = books + api_books
        unique_books = {book['DOI']: book for book in combined_books}.values()

        truncated_uris_found = {publisher['prefix'] for publisher in unique_publishers.values()}
        not_found_uris = [
            uri for uri in unique_truncated_uris if uri.replace('info:doi:', '')
            not in truncated_uris_found
        ]

        return {
            'publishers': list(unique_publishers.values()),
            'books': list(unique_books),
            'not found uris': not_found_uris
        }

    @staticmethod
    def fetch_data_from_apis(
            truncated_uri: str, not_found_books: pd.DataFrame
    ) -> dict[str, list[dict[str, Any]]]:
        """
        Fetch prefixes data from the APIs.
        Tries Crossref first, if not found then try with DataCite.

        Args:
            truncated_uri (str): The truncated work URI.
            not_found_books (pd.DataFrame): DataFrame containing books
            that were not found in the database.

        Returns:
            Dict[str, List[Dict[str, Any]]]: Data containing relevant metadata.
        """
        result = fetch_work_uri_data_from_crossref(truncated_uri)

        if result is None or result.get('message', {}) == 'Not Found':
            result = fetch_work_uri_data_from_datacite(truncated_uri)
            return DataProcessor.match_request_with_dois(
                result, 'datacite', not_found_books
            )

        return DataProcessor.match_request_with_dois(
            result['message'], 'crossref', not_found_books
        )

    @staticmethod
    def match_request_with_dois(
            items_resp: dict[str, Any] | None, source: str, not_found_books: pd.DataFrame
    ) -> dict[str, list[dict[str, Any]]]:
        """
        Fetch the books not found from the Identifiers API database,
        and save the publisher's names separately.

        Args:
            items_resp (dict[str, Any] | None): Dictionary containing response data.
            source (str): Source of the data ('crossref' or 'datacite').
            not_found_books (pd.DataFrame): DataFrame containing books that were not
            found in the database.

        Returns:
            Dict[str, List[Dict[str, Any]]]: Mapped metadata containing publishers and books.
        """
        result = {
            'publishers': [],
            'books': []
        }
        existing_prefixes = set()

        set_not_found_books = set(not_found_books['work_uri'])

        if source == 'crossref' and items_resp and items_resp.get('items'):
            for item in items_resp['items']:
                if item.get('DOI') and f"info:doi:{item.get('DOI')}" in set_not_found_books:
                    result['books'].append({
                        'DOI': item.get('DOI'),
                        'type': item.get('type')
                    })
            if items_resp.get('items'):
                prefix = items_resp['items'][0].get('prefix')
                if prefix and prefix not in existing_prefixes:
                    result['publishers'].append({
                        'publisher': items_resp['items'][0].get('publisher'),
                        'prefix': prefix
                    })
                    existing_prefixes.add(prefix)

        elif source == 'datacite' and items_resp:
            for item in items_resp['data']:
                if (item.get('attributes') and f"info:doi:{item['attributes'].get('doi')}"
                    in set_not_found_books):
                    if 'chapter' not in item['attributes']['types'].get('citeproc'):
                        result['books'].append({
                            'DOI': item['attributes'].get('doi'),
                            'type': item['attributes']['types'].get('citeproc')
                        })

            if items_resp.get('data'):
                prefix = items_resp['meta']['prefixes'][0]['id']
                if prefix and prefix not in existing_prefixes:
                    print()
                    result['publishers'].append({
                        'publisher': items_resp['data'][0]['attributes'].get('publisher'),
                        'prefix': prefix
                    })
                    existing_prefixes.add(prefix)

        return result


def main(date: datetime, timeline_execution: str) -> None:
    try:
        done = threading.Event()
        error_message = [None]
        processor = DataProcessor()

        loading_thread = threading.Thread(
            target=animate_loading, args=(done, error_message)
        )
        loading_thread.start()

        if timeline_execution == 'monthly':
            print('---> Executing on a monthly basis')
            execute_queries_monthly(date)
        else:
            print('---> Executing cumulative numbers')
            results, not_found_books = execute_queries_and_filter(date, '')

            results = processor.process_data(results, not_found_books)
            date_y_m = date.strftime('%Y-%m')
            save_dois_to_json(results['books'], f'output_files/{date_y_m}/books_{date_y_m}.json')
            save_dois_to_json(results['publishers'], f'output_files/{date_y_m}/publishers_{date_y_m}.json')
            save_dois_to_json(results['not found uris'], f'output_files/{date_y_m}/not_found_uris_{date_y_m}.json')

    except Exception as e:
        error_message[0] = ''.join(
            traceback.format_exception(None, e, e.__traceback__)
        )
    finally:
        done.set()
        loading_thread.join()

if __name__ == '__main__':
    """Change the date as needed"""
    date = datetime(2022, 12, 1).date()
    main(date, timeline_execution='monthly')
