import pandas as pd
import os
import requests

DOI = 'info:doi:10.1525/luminos.10'

def get_citation_data_from_doi(doi):
    """Fetches citations directly from the API endpoint."""
    url = f'https://metrics-api.operas-eu.org/citations?work_uri={doi}'
    response = requests.get(url)
    if response.status_code == 200:
        return response.json()['data']
    else:
        print(f'Error: {response.status_code}, {response.text}')


def combine_authors(authors):
    """Combine last names and initials from authors.
    Otherwise would be: [{'last_name': '', 'initial': ''}"""
    if isinstance(authors, list):
        return ', '.join(
            f"{author.get('last_name', '')} {author.get('initial', '')}"
            for author in authors if isinstance(author, dict)
            and author.get('last_name') and author.get('initial')
        )


def main():
    """Main function to fetch data and save it to a CSV."""
    output_file = os.getenv("OUTPUT_FILE", "output_files/output_webinar.csv")

    try:
        citations = get_citation_data_from_doi(DOI)
        if citations:
            # Create DataFrame from citations
            df = pd.DataFrame(citations)

            # Combine authors into a single string
            df['authors'] = df['authors'].apply(combine_authors)

            # Save to CSV
            df.to_csv(output_file, index=False)
            print(f"Data saved to {output_file}")
        else:
            print("No citations data to save.")
    except Exception as e:
        print(f"An error occurred during processing: {e}")

if __name__ == "__main__":
    main()
