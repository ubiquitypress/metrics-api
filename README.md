Set up docker for, e.g., Ubuntu, using the stable channel
=========================================================

Instructions cribbed from: https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository

    $ sudo apt-get install \
        apt-transport-https \
        ca-certificates \
        curl \
        software-properties-common
    $ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    $ sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"

Install Requirements
--------------
N.B. you might have to install setuptools in python3.10 with: 'pip3.10 install -U setuptools', otherwise installing psycopg2 won't work.

Install Docker
--------------
$ sudo apt-get update
$ sudo apt-get install docker-ce docker-compose

You may need to add yourself to the docker group thus:

$ sudo adduser $LOGNAME docker
$ exec newgrp docker    # or log out, log back in

Edit db.env file
----------------

$ cp db.env.example db.env
$ $EDITOR db.env

Set up environment
------------------

$ docker-compose up

Test
----

    $ env-vars ()
    {
        awk -F= '{printf "'%s=%s' ", $1, $2}' config/test_api.env
    }
    $ docker exec -it metrics_api env $(env-vars) nosetests -v

Setup locally env variables
---------------------------
    - Create a env file like 'metrices_api_webpy.bash', with the bellow content:
        #!/bin/bash
        
        # Just sets env variables then runs python for metrics-api project
        # usage `[this-script] path/to/metrics.py`
        
        export API_DEBUG='true'
        
        export POSTGRES_HOST=localhost
        export POSTGRES_PORT='54321'
        export POSTGRES_DB='metrics_api'
        export POSTGRES_USER=metrics_db_api_user
        export POSTGRES_PASSWORD=mysecretpassword
        
        export SECRET_KEY="dev-key-ok-as-is"
        export JWT_DISABLED=True
        
        export ALLOW_ORIGIN="*"
        
        export CACHE_LIFESPAN=60
        export REDIS_HOST=localhost
        
        
        export RMQ_HOST=""
        export RMQ_PASSWORD="your-password"
        export RMQ_PORT=5671
        export RMQ_USER="your-user"
        export RMQ_VHOST="your-vhost"
        
        
        ARGS=("$*")
        
        ${ARGS}
    

    $ source ~/.bash_scripts/metrices_api_webpy.bash
    

Set up locally DB
-----------------

    - ALL SCHEMAs:
            - $ for f in ./setup/metrics_db/sql/*.sql ; do psql -h localhost -U postgres -d $POSTGRES_DB -f $f ; done

    - TESTING: IF the event, doi_metadata and publishers need to be tested with dummy data:
            - $ for f in src/tests/fixtures/*.sql ; do psql -h localhost -U postgres -d $POSTGRES_DB -f $f ; done
