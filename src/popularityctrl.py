from datetime import date, timedelta

import web

from api import json_response, api_response
from aux import logger_instance, debug_mode
from cache import set_no_expire_cache_value, get_cache_value
from errors import NotFound, Error, BADPARAMS
from models.aggregation import Aggregation
from models.event import Event
from servicesctrl import get_service_works
from validation import build_date_clause, build_params


logger = logger_instance(__name__)
web.config.debug = debug_mode()


MEASURES = [  # Default - TODO: Make these configurable - probably via consul
    'https://metrics.operas-eu.org/crossref/citations/v1',
    'https://metrics.operas-eu.org/twitter/tweets/v1',
    'https://metrics.operas-eu.org/up-logs/downloads/v1',
    'https://metrics.operas-eu.org/up-logs/reads/v1',
    'https://metrics.operas-eu.org/up-logs/sessions/v1',
]

MEASURE_WEIGHTS = {  # Default - TODO: Make these configurable (consul)
    'https://metrics.operas-eu.org/crossref/citations/v1': 1,
    'https://metrics.operas-eu.org/twitter/tweets/v1': 1,
    'https://metrics.operas-eu.org/up-logs/downloads/v1': 1,
    'https://metrics.operas-eu.org/up-logs/reads/v1': 1,
    'https://metrics.operas-eu.org/up-logs/sessions/v1': 1,
}

WEIGHTS = [MEASURE_WEIGHTS[measure] for measure in MEASURES]


class PopularityController:
    """Handles actions related to popularity metrics"""

    @json_response
    @api_response
    def GET(self, *args):
        """Get list of most popular works for a given service"""
        service_code = web.input().get('service_code')

        if not service_code:
            raise Error(BADPARAMS, msg='service_code is required')

        popular_works = get_popular_works(service_code)

        if popular_works is None:
            raise NotFound

        return popular_works

    @json_response
    def OPTIONS(self):
        return


# TODO: I don't think this is being used anymore (from here, down)
#  Need to Confirm
#  ---------------

def get_metrics(work_uri, start_date, end_date):
    """Fetch metrics for a given work_uri.

    Args:
        work_uri (str): Uri of the work stored in the database.
        start_date (str): yyyy-mm-dd
        end_date (str): yyyy-mm-dd

    Returns:
        Dict: Metrics summary for the work specified.
    """
    filters = f'work_uri:{work_uri}'
    clause, params = build_params(filters)
    date_clause, date_params = build_date_clause(start_date, end_date)
    clause += date_clause
    params.update(date_params)

    criterion = 'measure_uri'
    query_args = [criterion, clause, params]

    if not Aggregation.is_allowed(criterion):
        raise Error(
            BADPARAMS,
            msg=f'Aggregation must be one of the following:'
                f' {Aggregation.list_allowed()}.'
        )

    aggregation = Aggregation(criterion)
    aggregation.data = Event.get_for_aggregation(*query_args)

    return aggregation.aggregate()


def sum_weighting(values, weights):
    """Apply positional weights to a list of values, then sum the result.

    Args:
        values (list): Total metrics counts for each measure.
        weights (list): How each value should be weighted for popularity score.

    Returns:
        int: Total weighted score of all metrics for a work.
    """
    new_values = [
        values[i] * weights[i]
        for i in range(len(weights))
    ]
    return sum(new_values)


def set_popularity_metrics(service_code):
    """Run daily to set the popularity metrics values in the cache"""

    uris = get_service_works(service_code)
    redis_key_base = f'popularity/{service_code}'
    today = date.today()
    start_date = (today - timedelta(days=365)).isoformat()
    end_date = today.isoformat()

    metrics = assemble_metrics(uris, start_date, end_date)

    metrics.sort(
        key=lambda x: sum_weighting(x[1:], WEIGHTS),
        reverse=True
    )

    popular_works = [work[0] for work in metrics[:10]]  # maybe include score?

    set_no_expire_cache_value(f'{redis_key_base}/metrics', popular_works)
    set_no_expire_cache_value(f'{redis_key_base}/last_checked', end_date)


def get_popular_works(service_code):
    redis_key = f'popularity/{service_code}/metrics'
    return get_cache_value(redis_key)


def assemble_metrics(uris, start_date, end_date):
    """Fetch metrics for all uris specified.

    Args:
        uris (list): (uri, id) tuples from servicesctrl.get_service_works().
        start_date (str): yyyy-mm-dd.
        end_date (str): yyyy-mm-dd.

    Returns:
        List: metrics for each URI, prepended with their preferred identifiers.

    Example of output: (Each value matches the order of MEASURES)
        [
            [id_1, 5, 84, 658, 70, 1951],
            [id_2, 9, 20, 635, 80, 1708],
            [id_3, 12, 27, 950, 73, 1834],
            [id_4, 13, 90, 730, 69, 1693],
            [id_5, 7, 12, 587, 78, 1824],
            ... <etc>,
        ]
    """
    metrics = []

    for uri, identifier in uris:
        data = get_metrics(uri, start_date, end_date)

        summarised = {
            result['measure_uri']: result['value']
            for result in data if result['measure_uri'] in MEASURES
        }
        metrics.append(
            [identifier] + [summarised.get(measure, 0) for measure in MEASURES]
        )

    return metrics
