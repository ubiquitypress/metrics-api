"""Contains classes, logic, etc. for tracking groups of works that belong to a
service (jornal, press, etc...)
"""

import json

import web

from api import json_response, api_response
from auth import valid_user
from aux import logger_instance, debug_mode
from cache import (
    delete_cache_value,
    get_cache_value,
    set_no_expire_cache_value,
)
from errors import BadRequest, NotFound

logger = logger_instance(__name__)
web.config.debug = debug_mode()

SERVICE_CODES_REDIS_KEY = 'services/codes'
SERVICE_WORKS_REDIS_KEY_BASE = 'services/works'


class ServicesController:
    """Handles actions related to popularity metrics."""

    @json_response
    @api_response
    def GET(self, name=None):
        service_code = web.input().get('service_code')
        result = get_service_works(service_code)

        if not result:
            raise NotFound

        return list(result)

    @json_response
    @api_response
    @valid_user
    def POST(self, name=None):
        """Add works to a service code."""
        try:
            data = json.loads(web.data().decode('utf-8'))

            service_code = data.get('service_code', '')
            if not service_code:
                raise BadRequest(msg='service_code is required')

            uris = data.get('uris', [])
            if not uris:
                raise BadRequest(msg='uris are required')
            elif uris_are_invalid(uris):
                raise BadRequest(
                    msg='invalid format of URIs - must be submitted in pairs'
                )

            service_works = add_service_works(service_code, uris)

            if service_works:
                add_cached_service_code(service_code)

        except Exception as e:  # exception is unclear at this point
            logger.error(e)
            raise e

        return '200'

    @json_response
    @api_response
    @valid_user
    def DELETE(self, name=None):  # Purge Cache of data linked to service code
        try:
            data = json.loads(web.data().decode('utf-8'))
            service_code = data.get('service_code', '')

            if not service_code:
                raise BadRequest

            uris = data.get('uris', [])
            if uris:
                works = remove_service_works(service_code, uris)
                msg = f'{len(uris)} works deleted from service, {service_code}'

            else:
                works = None

            if not works:
                remove_cached_service_code(service_code)
                msg = f'All works deleted from service, {service_code}'

        except Exception as e:  # exception is unclear at this point
            logger.error(e)
            raise e

        return msg

    @json_response
    def OPTIONS(self):
        return


# ## URI List structure ##


def uris_are_invalid(uris):
    """Check that URIs are submitted in a format required.

    {  # Structure of data from post/put requests.
        code: xxx,
        uris: [
            (xxx1, yyy1),
            (xxx2, yyy2),
            (xxx3, yyy3)
        ],
    }

    Where, xxx is the format of a given URI in the metrics-API, and yyy is the
    format required by the service that will query the metrics-api.
    """
    try:
        for _, _ in uris:
            pass
    except (TypeError, ValueError):
        return True

    return False


# ## Cached Service Codes ##


def get_cached_service_codes():
    """Get list of service codes registered in the cache."""
    service_codes = get_cache_value(SERVICE_CODES_REDIS_KEY) or []

    return set(service_codes)


def add_cached_service_code(service_code):
    """Remove service code from cached list."""
    service_codes = set(get_cached_service_codes())
    service_codes.add(service_code)
    set_no_expire_cache_value(SERVICE_CODES_REDIS_KEY, list(service_codes))

    return service_codes


def remove_cached_service_code(service_code):
    """Remove service code and associated works from cached list."""
    redis_key = f'{SERVICE_WORKS_REDIS_KEY_BASE}/{service_code}'
    delete_cache_value(redis_key)
    service_codes = get_cached_service_codes()
    service_codes.discard(service_code)
    set_no_expire_cache_value(SERVICE_CODES_REDIS_KEY, list(service_codes))

    return service_codes


# ## Cached Service Works ##


def get_service_works(service_code):
    """Get list of works registered to a given service."""
    redis_key = f'{SERVICE_WORKS_REDIS_KEY_BASE}/{service_code}'
    service_works = get_cache_value(redis_key) or []

    return set(map(tuple, service_works))


def add_service_works(service_code, works):
    """Add a list of works to a service."""
    service_works = get_service_works(service_code)
    service_works.update(map(tuple, works))
    redis_key = f'{SERVICE_WORKS_REDIS_KEY_BASE}/{service_code}'
    set_no_expire_cache_value(redis_key, list(service_works))

    return service_works


def remove_service_works(service_code, works):
    """Remove works from a service."""
    service_works = get_service_works(service_code)
    service_works.difference_update(map(tuple, works))
    redis_key = f'{SERVICE_WORKS_REDIS_KEY_BASE}/{service_code}'
    set_no_expire_cache_value(redis_key, list(service_works))

    return service_works
