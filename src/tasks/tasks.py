from datetime import datetime
import logging
from os import getenv

from celery import Celery
from celery.schedules import crontab

from consul import get_trending_content_settings
from doi_metadata_logic import save_new_citation_event_metadata
from models.citation import DoiMetadata
from models.popularity import set_popularity_metrics
from servicesctrl import get_cached_service_codes, get_service_works
from publishersctrl import set_publishers


RMQ_AMQ_SCHEME = getenv('RMQ_AMQ_SCHEME', 'amqps')
RMQ_USER = getenv('RMQ_USER')
RMQ_PASSWORD = getenv('RMQ_PASSWORD')
RMQ_HOST = getenv('RMQ_HOST')
RMQ_PORT = getenv('RMQ_PORT', '5672')
RMQ_VHOST = getenv('RMQ_VHOST')

AMQ_URL = '{amq_scheme}://{user}:{password}@{host}:{port}/{vhost}'
CELERY_BROKER_URL = AMQ_URL.format(
    amq_scheme=RMQ_AMQ_SCHEME,
    user=RMQ_USER,
    password=RMQ_PASSWORD,
    host=RMQ_HOST,
    port=RMQ_PORT,
    vhost=RMQ_VHOST
)


celery_app = Celery('tasks', backend=None, broker=CELERY_BROKER_URL)

celery_app.conf.timezone = 'Europe/London'
celery_app.conf.beat_schedule = {
    'calculate-popular-works-every-day': {
        'task': 'refresh-all-popularity-metrics',
        'schedule': crontab(minute=30, hour=10, day_of_week='*')
    },
    'fetch-new-dois-metadata-daily': {
        'task': 'fetch-new-dois-metadata',
        'schedule': crontab(minute=0, hour=2, day_of_week='*')
    },
    'retrieve-publishers-every-month': {
        'task': 'retrieve-publishers',
        'schedule': crontab(hour=0, minute=0, day_of_month=1),
    }
}

celery_task_routes = {
    'calculate-service-popularity-metrics': {
        'queue': 'metrics-api.calculate-service-popularity-metrics'
    },
    'refresh-all-popularity-metrics': {
        'queue': 'metrics-api.refresh-all-popularity-metrics'
    },
    'fetch-doi-metadata': {
        'queue': 'metrics-api.fetch-doi-metadata'
    },
    'fetch-new-dois-metadata': {
        'queue': 'metrics-api.fetch-new-dois-metadata'
    },
    'retrieve-publishers': {
        'queue': 'metrics-api.retrieve-publishers'
    },
}

celery_app.conf.task_routes = celery_task_routes


@celery_app.task(name='retrieve-publishers')
def retrieve_publishers():
    set_publishers(datetime.now())


# Add temporary rate limit so as not to strain the Database
@celery_app.task(name='calculate-service-popularity-metrics', rate_limit='1/m')
def calculate_service_popularity_metrics(service_code):
    work_uri_and_external_id = get_service_works(service_code)
    weights = get_trending_content_settings(service_code)
    set_popularity_metrics(work_uri_and_external_id, weights, service_code)


@celery_app.task(name='refresh-all-popularity-metrics')
def refresh_all_popularity_metrics():
    for service_code in get_cached_service_codes():
        calculate_service_popularity_metrics.delay(service_code)


@celery_app.task(name='fetch-doi-metadata', rate_limit='1/s')
def fetch_doi_metadata(doi):
    result = save_new_citation_event_metadata(doi)
    if result:
        logging.info(f'Fetched metadata: {result}')


@celery_app.task(name='fetch-new-dois-metadata')
def fetch_new_dois_metadata():
    """Get all new dois with no metadata related and save them."""
    for doi in DoiMetadata.get_distinct_dois_without_metadata():
        fetch_doi_metadata.delay(doi)
