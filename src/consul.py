import os
from typing import Dict

from up_consul import ConsulClient

from aux import logger_instance

logger = logger_instance(__name__)

CONSUL_HOST = os.getenv('CONSUL_HOST')
CONSUL_TOKEN = os.getenv('CONSUL_TOKEN')


def get_trending_content_settings(service_code: str) -> Dict:
    consul_client = ConsulClient(
        host=CONSUL_HOST,
        token=CONSUL_TOKEN,
    )

    if '-j-' in service_code:
        path_prefix = 'journal'
    elif '-p-' in service_code:
        path_prefix = 'press'
    else:
        return

    prefix = f'{path_prefix}/{service_code}'
    trending_key_base = f'{prefix}/trending_content'

    try:
        settings = consul_client.kv.get_many(trending_key_base)
        return consul_client.kv.compile_service_settings(
            trending_key_base,
            settings,
        )
    except Exception as e:
        logger.error(f'Not found {e}')
