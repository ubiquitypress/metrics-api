import unittest
from unittest.mock import patch
from consul import get_trending_content_settings


class TestConsul(unittest.TestCase):

    @patch('consul.ConsulClient')
    def test_get_trending_content_settings_journal(self, MockConsulClient):
        # Arrange
        service_code = 'service-j-123'
        mock_client = MockConsulClient.return_value
        mock_client.kv.get_many.return_value = {'key': 'value'}
        mock_client.kv.compile_service_settings.return_value = {'compiled': 'settings'}

        # Act
        result = get_trending_content_settings(service_code)

        # Assert
        self.assertEqual(result, {'compiled': 'settings'})
        mock_client.kv.get_many.assert_called_once_with('journal/service-j-123/trending_content')

    @patch('consul.ConsulClient')
    def test_get_trending_content_settings_press(self, MockConsulClient):
        # Arrange
        service_code = 'service-p-456'
        mock_client = MockConsulClient.return_value
        mock_client.kv.get_many.return_value = {'key': 'value'}
        mock_client.kv.compile_service_settings.return_value = {'compiled': 'settings'}

        # Act
        result = get_trending_content_settings(service_code)

        # Assert
        self.assertEqual(result, {'compiled': 'settings'})
        mock_client.kv.get_many.assert_called_once_with('press/service-p-456/trending_content')

    @patch('consul.ConsulClient')
    def test_get_trending_content_settings_invalid_service_code(self, MockConsulClient):
        # Arrange
        service_code = 'service-x-789'  # Invalid service code
        mock_client = MockConsulClient.return_value

        # Act
        result = get_trending_content_settings(service_code)

        # Assert
        self.assertIsNone(result)

    @patch('consul.ConsulClient')
    def test_get_trending_content_settings_exception(self, MockConsulClient):
        # Arrange
        service_code = 'service-j-123'
        mock_client = MockConsulClient.return_value
        mock_client.kv.get_many.side_effect = Exception('Some error')

        # Act
        result = get_trending_content_settings(service_code)

        # Assert
        self.assertIsNone(result)
