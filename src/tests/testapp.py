import jwt
from paste.fixture import TestApp
from api import app


TOKEN_KEY = 'simpletokenkey'


class App(TestApp):
    _token = ''

    def __init__(self):
        middleware = []
        self.auth_headers = self.get_auth_headers(self._token)
        TestApp.__init__(self, app.wsgifunc(*middleware))

    def get_auth_headers(self, token):
        return {
            "Authorization": f"Bearer {token}",
            "Content-Type": "application/json"
        }


class AuthApp(App):
    token_content = {
        'authority': 'user',
        'email': 'javi@openbookpublishers.com',
        'name': 'OBP-tech',
        'sub': 'acct:javi@openbookpublishers.com'
    }
    _token = jwt.encode(
        token_content,
        TOKEN_KEY,
        algorithm="HS256",
    )


class AdminApp(App):
    token_content = {
        'authority': 'admin',
        'email': 'admin@openbookpublishers.com',
        'name': 'OBP-admin',
        'sub': 'acct:admin@openbookpublishers.com'
    }
    _token = jwt.encode(
        token_content,
        TOKEN_KEY,
        algorithm="HS256",
    )
