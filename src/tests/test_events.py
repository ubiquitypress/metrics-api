import json
from contextlib import contextmanager
import unittest
from unittest import mock

import web

from api import urls, db
from web import application


@contextmanager
def unauthorised_request():
    import auth
    auth.JWT_DISABLED = False

    try:
        yield None
    finally:
        auth.JWT_DISABLED = True


TEST_EVENT = {
    "work_uri": "info:doi:10.11647/obp.0020",
    "measure_uri": "https://metrics.operas-eu.org/world-reader/users/v1",
    "timestamp": "2019-01-01T01:00:00",
    "country_uri": "urn:iso:std:3166:-2:ES",
    "value": 512
}
DEFAULT_SECRET_KEY = 'simpletokenkey'


class TestEventsController(unittest.TestCase):
    def setUp(self):
        """Set up the test environment and app"""
        # Create a test application
        self.app = application(urls, globals())
        self.authapp = self.create_auth_app()

        # Set up web.ctx and web.input
        web.ctx = mock.MagicMock()
        web.ctx.status = '200 OK'
        web.ctx.headers = [('Content-Type', 'application/json;charset=UTF-8')]

        # Set up any other necessary context attributes
        web.ctx.env = {
            'REQUEST_METHOD': 'GET',
            'PATH_INFO': '/events',
            'QUERY_STRING': 'filter=work_uri:tag:ubiquitypress.com'
        }

    def create_auth_app(self):
        """Mock or create an authentication app"""
        class MockAuthApp:
            @property
            def auth_headers(self):
                # Mock authentication headers
                return {"Authorization": f"Bearer {DEFAULT_SECRET_KEY}"}

        return MockAuthApp()

    def test_get_event_from_id(self):
        """Get an event id from the dB and test it is returned by the API"""
        query = 'SELECT event_id FROM event LIMIT 1'
        event_id = db.query(query)
        event_id = [id['event_id'] for id in event_id][0]
        web.input = mock.MagicMock(return_value={'event_id': event_id})

        res = self.app.request(f'/events?event_id={event_id}', method='GET')
        results = json.loads(res.data)

        self.assertEqual(results["code"], 200)
        self.assertEqual(results["status"], "ok")
        self.assertEqual(results["count"], 1)
        self.assertEqual(results["data"][0]["event_id"], event_id)
        self.assertEqual(
            results["data"][0]["work_uri"],
            "tag:ubiquitypress.com,2024|press|book:up-p-testing-300"
        )
        self.assertEqual(
            results["data"][0]["measure_uri"],
            "https://metrics.operas-eu.org/world-reader/users/v1"
        )
        self.assertEqual(
            results["data"][0]["country_uri"], "urn:iso:std:3166:-2:ZW"
        )

    def test_get_all_events_from_uri(self):
        """It should return only 4 as strict match in the dB"""
        uri = "tag:ubiquitypress.com"
        web.input = mock.MagicMock(return_value={'filter': f'work_uri:{uri}'})
        res = self.app.request(f'/events?filter=work_uri:{uri}', method='GET')
        results = json.loads(res.data)

        self.assertEqual(results["code"], 200)
        self.assertEqual(results["status"], "ok")
        self.assertEqual(results["count"], 4)
        self.assertEqual(results["data"][2]["work_uri"], uri)
        self.assertTrue(results["data"])

    def test_get_all_events_from_fake_uri(self):
        """With a fake uri will raise error NORESULT."""
        uri = "info:doi:10.11647/obp.FAKE"
        web.input = mock.MagicMock(return_value={'filter': f'work_uri:{uri}'})
        res = self.app.request(f'/events?filter=work_uri:{uri}', method='GET')
        results = json.loads(res.data)

        self.assertEqual(results["code"], 404)
        self.assertEqual(results["status"], "error")
        self.assertEqual(results["count"], 0)

    def test_get_all_events_from_uri_and_start_date(self):
        uri = "info:doi:10.11647/obp.0002"
        start_date = "2024-01-01"
        web.input = mock.MagicMock(
            return_value={'filter': f'work_uri:{uri}', 'start_date': start_date}
        )
        res = self.app.request(
            f'/events?filter=work_uri:{uri}&start_date={start_date}',
            method='GET'
        )
        results = json.loads(res.data)

        self.assertEqual(results["code"], 200)
        self.assertEqual(results["status"], "ok")
        self.assertEqual(results["count"], 1)
        self.assertEqual(results["data"][0]["work_uri"], uri)
        self.assertTrue(results["data"])

    def test_get_from_uri_aggregate_measure_uri(self):
        """Return all the events for a specific work_uri
        with not repeated measure_uri."""
        uri = "tag:ubiquitypress.com"
        web.input = mock.MagicMock(
            return_value={'aggregation': 'measure_uri', 'filter': f'work_uri:{uri}'}
        )
        res = self.app.request(
            f'/events?aggregation=measure_uri&filter=work_uri:{uri}',
            method='GET'
        )
        results = json.loads(res.data)

        self.assertEqual(results["code"], 200)
        self.assertEqual(results["status"], "ok")
        self.assertEqual(results["count"], 3)
        expected = {
            "World Reader": 200,
            "Crossref": 500,
            "Open Edition": 300
        }
        for value in results["data"]:
            self.assertEqual(value["value"], expected[value["source"]])

    def test_get_from_uri_aggregate_country_measure(self):
        uri = "tag:ubiquitypress.com,2024|press|book:up-p-testing-300"
        web.input = mock.MagicMock(
            return_value={
                'aggregation': 'measure_uri,country_uri', 'filter': f'work_uri:{uri}'
            }
        )
        res = self.app.request(
            f'/events?aggregation=measure_uri,country_uri&filter=work_uri:{uri}',
            method='GET'
        )
        results = json.loads(res.data)

        self.assertEqual(results["code"], 200)
        self.assertEqual(results["status"], "ok")
        self.assertEqual(results["count"], 3)
        expected = {
            "World Reader": 1,
            "Crossref": 2,
            "Unglue.it": 1
        }
        for value in results["data"]:
            self.assertEqual(len(value["data"]), expected[value["source"]])

    def test_post_unauth(self):
        with unauthorised_request():
            res = self.app.request(f'/events', method='POST')
            results = json.loads(res.data)
            self.assertEqual(results["status"], "error")
            self.assertEqual(results["code"], 403)
            self.assertEqual(
                results["message"],
                "You do not have permissions to access this resource."
            )

    def test_post_auth_trivial(self):
        headers = self.authapp.auth_headers
        res = self.app.request(
            f'/events', method='POST',
            data=json.dumps(TEST_EVENT), headers=headers
        )
        results = json.loads(res.data)
        self.assertEqual(results["status"], "ok")
        self.assertEqual(results["code"], 200)

    def test_post_auth_readback(self):
        headers = self.authapp.auth_headers
        res = self.app.request(
            f'/events', method='POST',
            data=json.dumps(TEST_EVENT), headers=headers
        )
        results = json.loads(res.data)
        self.assertEqual(results["status"], "ok")
        for key, value in TEST_EVENT.items():
            self.assertEqual(results["data"][0][key], TEST_EVENT[key])
        self.assertEqual(
            results["data"][0]["uploader_uri"],
            "acct:jwt-disabled@metrics-api.operas-eu.org"
        )

    def test_not_allowed_put(self):
        res = self.app.request(f'/events', method='PUT')
        results = json.loads(res.data)
        self.assertEqual(results["status"], "error")
        self.assertEqual(results["code"], 405)

    def test_not_allowed_delete(self):
        res = self.app.request(f'/events', method='DELETE')
        results = json.loads(res.data)
        self.assertEqual(results["status"], "error")
        self.assertEqual(results["code"], 405)
