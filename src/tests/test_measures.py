import json
import unittest

from api import urls
from web import application


class TestMeasures(unittest.TestCase):
    def setUp(self):
        self.app = application(urls, globals())

    def test_get_all_measures(self):
        res = self.app.request('/measures', method='GET')
        results = json.loads(res.data)
        self.assertEqual(results["status"], "ok")
        self.assertTrue(results["data"])

    def test_not_allowed_post(self):
        res = self.app.request('/measures', method='POST')
        results = json.loads(res.data)
        self.assertEqual(results["status"], "error")
        self.assertEqual(results["code"], 405)

    def test_not_allowed_put(self):
        res = self.app.request('/measures', method='PUT')
        results = json.loads(res.data)
        self.assertEqual(results["status"], "error")
        self.assertEqual(results["code"], 405)

    def test_not_allowed_delete(self):
        res = self.app.request('/measures', method='DELETE')
        results = json.loads(res.data)
        self.assertEqual(results["status"], "error")
        self.assertEqual(results["code"], 405)
