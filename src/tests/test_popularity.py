import string
from datetime import date, timedelta
import json
import random
import unittest
from unittest import mock
from unittest.mock import patch

import web
from web import ctx, application

from api import urls
from models.popularity import (
    group_metrics_by_work_uri,
    calculate_the_weighted_score,
    get_top_10_external_ids,
    set_popularity_metrics,
    get_books_or_journal_weights
)


def get_uri_to_id_mapping():
    """
    Mapping setup by the /services endpoint
    and saved in the cache.
    """
    return [
        ("info:doi:10.5334/bav", 1),
        ("info:doi:10.5334/baw", 2),
        ("info:doi:10.5334/bax", 3),
        ("info:doi:10.5334/bay", 4),
        ("info:doi:10.5334/baz", 5)
    ]


def get_weights():
    """Weights returned by CONSUL"""
    return  {
        "books": {
            "timeframe": 180,
            "weightings": {
                "citations": 20,
                "downloads": 30,
                "reads": 40,
                "views": 10
            }
        }
    }


def generate_random_work_uri(length=3):
    """Generate a random work_uri consisting of lowercase letters."""
    return ''.join(random.choice(string.ascii_lowercase) for _ in range(length))


def generate_scored_works(num_elements=15):
    scored_works = []

    for _ in range(num_elements):
        work_uri = generate_random_work_uri()
        score = random.randint(1, 100)  # Score between 1 and 100
        external_id = str(random.randint(100, 999))  # Random external_id between 100 and 999
        scored_works.append({'work_uri': work_uri, 'score': score, 'external_id': external_id})

    return scored_works


class TestPopularity(unittest.TestCase):
    def setUp(self):
        """Set up the test environment and app"""
        # Create a test application
        self.app = application(urls, globals())
        web.ctx = mock.MagicMock()
        ctx.status = '200 OK'
        ctx.headers = [('Content-Type', 'application/json;charset=UTF-8')]

        # Set up any other necessary context attributes
        web.ctx.env = {
            'REQUEST_METHOD': 'GET',
            'PATH_INFO': '/popularity',
            'QUERY_STRING': 'filter=service_code:not_important'
        }

    def test_group_metrics_by_work_uri(self) -> None:
        """
        Convert the input list of dict to a dict with the wor_uris as
        keys pointing to their values organized.
        """
        works_data = [
            {
                'work_uri': 'info:doi:10.1234/abc',
                'type': 'citations', 'total': 100
            },
            {
                'work_uri': 'info:doi:10.1234/abc',
                'type': 'downloads', 'total': 200
            },
            {
                'work_uri': 'info:doi:10.5678/xyz',
                'type': 'citations', 'total': 50
            },
        ]

        expected_output = {
            'info:doi:10.1234/abc': {'citations': 100, 'downloads': 200},
            'info:doi:10.5678/xyz': {'citations': 50},
        }

        result = group_metrics_by_work_uri(works_data)
        self.assertEqual(result, expected_output)

    def test_calculate_the_weighted_score(self) -> None:
        """
        Multiply the types by the weightings and sum them
        """
        grouped_data = {
            'info:doi:10.1234/abc': {
                'citations': 100,
                'downloads': 200
            },
            'info:doi:10.5678/xyz': {'citations': 50}
        }

        weightings = {
            'citations': 0.7,
            'downloads': 0.3
        }

        uri_to_id_mapping = {
            'info:doi:10.1234/abc': '123',
            'info:doi:10.5678/xyz': '456'
        }

        expected_output = [
            {
                'work_uri': 'info:doi:10.1234/abc',
                'score': 100 * 0.7 + 200 * 0.3,
                'external_id': '123'
            },
            {
                'work_uri': 'info:doi:10.5678/xyz',
                'score': 50 * 0.7,
                'external_id': '456'
            }
        ]

        result = calculate_the_weighted_score(
            grouped_data, weightings, uri_to_id_mapping
        )
        self.assertEqual(result, expected_output)

    def test_calculate_the_weighted_score_join_views_sessions(
            self
    ) -> None:
        """
        Multiply the types by the weightings and sum them
        Also join views and sessions
        """
        grouped_data = {
            'info:doi:10.1234/abc': {
                'views': 100,
                'downloads': 200
            },
            'info:doi:10.5678/def': {'sessions': 50},
            'info:doi:10.5678/ghi': {
                'sessions': 50, 'views': 10
            }
        }

        weightings = {
            'citations': 0.7,
            'downloads': 0.3,
            'views': 5
        }

        uri_to_id_mapping = {
            ('info:doi:10.1234/abc', '123'),
            ('info:doi:10.5678/def', '456'),
            ('info:doi:10.5678/ghi', '789')
        }

        expected_output = [
            {
                'work_uri': 'info:doi:10.1234/abc',
                'score': 100 * 5 + 200 * 0.3,
                'external_id': '123'
            },
            {
                'work_uri': 'info:doi:10.5678/def',
                'score': 50 * 5.0,
                'external_id': '456'
            },
            {
                'work_uri': 'info:doi:10.5678/ghi',
                'score': 60 * 5,
                'external_id': '789'
            }
        ]

        result = calculate_the_weighted_score(
            grouped_data, weightings, uri_to_id_mapping
        )
        self.assertEqual(result, expected_output)

    def test_get_top_10_external_ids(self) -> None:
        """
        The expected result is getting the 3 top ids after the calculation
        from calculate_the_weighted_score and ignoring the rest of the data.
        """
        scored_works = generate_scored_works()

        result = get_top_10_external_ids(scored_works)
        self.assertEqual(len(result), 10)

    @patch('models.popularity.set_no_expire_cache_value')
    @patch('models.popularity.get_top_10_external_ids')
    @patch('models.popularity.calculate_the_weighted_score')
    @patch('models.popularity.group_metrics_by_work_uri')
    @patch('models.popularity.Popularity.get_metrics_by_type')
    def test_set_popularity_metrics(
            self,
            mock_get_metrics_by_type,
            mock_group_metrics_by_work_uri,
            mock_calculate_weighted_score,
            mock_get_top_3_ids,
            mock_set_cache_value
    ):
        # Setup mock return values for each function call
        mock_get_metrics_by_type.popularity.return_value = [
            {'work_uri': 'info:doi:10.1234/abc', 'type': 'citations', 'total': 100},
            {'work_uri': 'info:doi:10.1234/abc', 'type': 'downloads', 'total': 200}
        ]
        mock_group_metrics_by_work_uri.return_value = {
            'info:doi:10.1234/abc': {'citations': 100, 'downloads': 200}
        }
        mock_calculate_weighted_score.return_value = [
            {'work_uri': 'info:doi:10.1234/abc', 'score': 70, 'external_id': '123'}
        ]
        mock_get_top_3_ids.return_value = ['123', '456', '789']

        uris_and_ids = get_uri_to_id_mapping()
        weights = get_weights()
        # Method to test
        set_popularity_metrics(
            uris_and_ids, weights, service_code='up-p-testing'
        )

        # Get the dates
        today = date.today()
        start_date = (
            today - timedelta(days=weights['books']['timeframe'])
        ).isoformat()
        end_date = today.isoformat()

        # Verify that the function calls were made with the correct arguments
        mock_get_metrics_by_type.assert_called_once_with(
            (
                 'info:doi:10.5334/bav',
                 'info:doi:10.5334/baw',
                 'info:doi:10.5334/bax',
                 'info:doi:10.5334/bay',
                 'info:doi:10.5334/baz'
             ),
            start_date, end_date
        )
        mock_group_metrics_by_work_uri.assert_called_once_with(
            mock_get_metrics_by_type.return_value
        )
        mock_calculate_weighted_score.assert_called_once_with(
            mock_group_metrics_by_work_uri.return_value,
            weights['books']['weightings'],
            uris_and_ids
        )
        mock_get_top_3_ids.assert_called_once_with(
            mock_calculate_weighted_score.return_value
        )

        # Check that cache values were set correctly
        redis_key_base = f'popularity/up-p-testing'
        mock_set_cache_value.assert_any_call(
            f'{redis_key_base}/metrics', mock_get_top_3_ids.return_value
        )
        mock_set_cache_value.assert_any_call(f'{redis_key_base}/last_checked', end_date)

    @patch('popularityctrl.get_popular_works')
    def test_get_popular_demo(self, popular_function):
        """Test for getting popular works, mock the function"""
        popular_function.return_value = [1, 2, 3]

        # Mock web.input to simulate the query parameters
        web.input = mock.MagicMock(return_value={'service_code': 'not_important'})

        res = self.app.request(
            '/popularity?service_code=not_important', method='GET'
        )
        results = json.loads(res.data)

        self.assertEqual(results["status"], "ok")
        self.assertEqual(results["code"], 200)
        self.assertEqual(results["data"], [1, 2, 3])

    def test_not_allowed_post(self):
        """Test that POST is not allowed for the /popularity endpoint"""
        res = self.app.request('/popularity', method='POST')
        results = json.loads(res.data.decode('utf-8'))

        self.assertEqual(results["status"], "error")
        self.assertEqual(results["code"], 405)

    def test_not_allowed_put(self):
        """Test that PUT is not allowed for the /popularity endpoint"""
        res = self.app.request('/popularity', method='PUT')
        results = json.loads(res.data.decode('utf-8'))

        self.assertEqual(results["status"], "error")
        self.assertEqual(results["code"], 405)

    def test_not_allowed_delete(self):
        """Test that DELETE is not allowed for the /popularity endpoint"""
        res = self.app.request('/popularity', method='DELETE')
        results = json.loads(res.data.decode('utf-8'))

        self.assertEqual(results["status"], "error")
        self.assertEqual(results["code"], 405)

    def test_get_journal_weights(self):
        service_code = 'abc-j-123'
        weights = {'journals': {'citations': 5, 'downloads': 10}}
        result = get_books_or_journal_weights(service_code, weights)
        expected = {'citations': 5, 'downloads': 10}
        self.assertEqual(result, expected)

    def test_get_book_weights(self):
        service_code = 'xyz-p-456'
        weights = {'books': {'citations': 3, 'downloads': 7}}
        result = get_books_or_journal_weights(service_code, weights)
        expected = {'citations': 3, 'downloads': 7}
        self.assertEqual(result, expected)

    def test_get_books_or_journal_empty_weights(self):
        service_code = 'abc-p-789'
        weights = {}
        result = get_books_or_journal_weights(service_code, weights)
        expected = {
            'weightings': {
                'citations': 1,
                'downloads': 1,
                'reads': 1,
                'views': 1
            },
            'timeframe': 365
        }
        self.assertEqual(result, expected)

    def test_no_service_code(self):
        service_code = ''
        weights = {}
        result = get_books_or_journal_weights(service_code, weights)
        expected = {
            'weightings': {
                'citations': 1,
                'downloads': 1,
                'reads': 1,
                'views': 1
            },
            'timeframe': 365
        }
        self.assertEqual(result, expected)
