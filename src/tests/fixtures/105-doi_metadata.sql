                                                                          INSERT INTO doi_metadata (doi, metadata)
SELECT DISTINCT
    event_uri AS doi,
    jsonb_build_object(
        'authors', '[{"name": "John Doe"}]'::JSONB,
        'editors', '[{"name": "Jane Smith"}]'::JSONB,
        'year', EXTRACT(YEAR FROM timestamp),
        'title', 'Dummy Title',
        'source', 'Dummy Source',
        'volume', 'Dummy Volume',
        'issue', 'Dummy Issue',
        'page', '1-10',
        'doi', event_uri,
        'type', 'journal-article',
        'published', jsonb_build_object(
            'date-parts', jsonb_build_array(
                jsonb_build_array(EXTRACT(YEAR FROM timestamp)::INT, 1, 1)
            )
        )
    )
FROM event
WHERE work_uri = 'tag:ubiquitypress.com'
  AND measure_uri = 'https://metrics.operas-eu.org/crossref/citations/v1'
ON CONFLICT (doi) DO NOTHING;
