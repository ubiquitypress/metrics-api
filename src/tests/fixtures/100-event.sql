INSERT INTO event (
    event_id, work_uri, measure_uri, timestamp, value, event_uri,
    country_uri, uploader_uri
) VALUES
(gen_random_uuid(),
    'tag:ubiquitypress.com,2024|press|book:up-p-testing-300',
    'https://metrics.operas-eu.org/world-reader/users/v1',
    '2024-01-01 00:00:00', 100, 'event_uri_1', 'urn:iso:std:3166:-2:ZW',
    'user1@example.com'),
(gen_random_uuid(),
    'tag:ubiquitypress.com',
    'https://metrics.operas-eu.org/world-reader/pageviews/v1',
    '2024-02-01 00:00:00', 200, 'event_uri_2', 'urn:iso:std:3166:-2:ZM',
    'user2@example.com'),
(gen_random_uuid(),
    'tag:ubiquitypress.com,2024|press|book:up-p-testing-3',
    'https://metrics.operas-eu.org/wikimedia/views/v1',
    '2024-03-01 00:00:00', 150, 'event_uri_3', 'urn:iso:std:3166:-2:ZA',
    'user3@example.com'),
(gen_random_uuid(),
    'tag:ubiquitypress.com,2024|press|book:up-p-testing-300',
    'https://metrics.operas-eu.org/unglueit/downloads/v1',
    '2024-04-01 00:00:00', 250, 'event_uri_4', 'urn:iso:std:3166:-2:YT',
    'user4@example.com'),
(gen_random_uuid(),
    'tag:ubiquitypress.com',
    'https://metrics.operas-eu.org/open-edition/views/v1',
    '2024-05-01 00:00:00', 300, 'event_uri_5', 'urn:iso:std:3166:-2:YE',
    'user5@example.com'),
(gen_random_uuid(),
    'tag:ubiquitypress.com,2024|press|book:up-p-testing-300',
    'https://metrics.operas-eu.org/crossref/citations/v1',
    '2024-01-01 00:00:00', 100, 'event_uri_random_1', 'urn:iso:std:3166:-2:ZW',
    'user1@example.com'),
(gen_random_uuid(),
    'tag:ubiquitypress.com',
    'https://metrics.operas-eu.org/crossref/citations/v1',
    '2024-02-01 00:00:00', 200, 'event_uri_random_2', 'urn:iso:std:3166:-2:ZM',
    'user2@example.com'),
(gen_random_uuid(),
    'tag:ubiquitypress.com,2024|press|book:up-p-testing-3',
    'https://metrics.operas-eu.org/crossref/citations/v1',
    '2024-03-01 00:00:00', 150, 'event_uri_random_3', 'urn:iso:std:3166:-2:ZA',
    'user3@example.com'),
(gen_random_uuid(),
    'tag:ubiquitypress.com,2024|press|book:up-p-testing-300',
    'https://metrics.operas-eu.org/crossref/citations/v1',
    '2024-04-01 00:00:00', 250, 'event_uri_random_4', 'urn:iso:std:3166:-2:YT',
    'user4@example.com'),
(gen_random_uuid(),
    'tag:ubiquitypress.com',
    'https://metrics.operas-eu.org/crossref/citations/v1',
    '2024-05-01 00:00:00', 300, 'event_uri_random_5', 'urn:iso:std:3166:-2:YE',
    'user5@example.com'),
(gen_random_uuid(),
    'info:doi:10.11647/obp.0002',
    'https://metrics.operas-eu.org/world-reader/users/v1',
    '2024-06-01 00:00:00', 120, 'event_uri_6', 'urn:iso:std:3166:-2:ZW',
    'user6@example.com'),
(gen_random_uuid(),
    'info:doi:10.11647/obp.0003',
    'https://metrics.operas-eu.org/world-reader/pageviews/v1',
    '2024-07-01 00:00:00', 220, 'event_uri_7', 'urn:iso:std:3166:-2:ZM',
    'user7@example.com'),
(gen_random_uuid(),
    'info:doi:10.11647/obp.0004',
    'https://metrics.operas-eu.org/wikimedia/views/v1',
    '2024-08-01 00:00:00', 180, 'event_uri_8', 'urn:iso:std:3166:-2:ZA',
    'user8@example.com'),
(gen_random_uuid(),
    'info:doi:10.11647/obp.0005',
    'https://metrics.operas-eu.org/unglueit/downloads/v1',
    '2024-09-01 00:00:00', 260, 'event_uri_9', 'urn:iso:std:3166:-2:YT',
    'user9@example.com'),
(gen_random_uuid(),
    'info:doi:10.11647/obp.0006',
    'https://metrics.operas-eu.org/open-edition/views/v1',
    '2024-10-01 00:00:00', 320, 'event_uri_10', 'urn:iso:std:3166:-2:YE',
    'user10@example.com'),
(gen_random_uuid(),
    'info:doi:10.11647/obp.0007',
    'https://metrics.operas-eu.org/crossref/citations/v1',
    '2024-06-01 00:00:00', 130, 'event_uri_random_6', 'urn:iso:std:3166:-2:ZW',
    'user6@example.com'),
(gen_random_uuid(),
    'info:doi:10.11647/obp.0008',
    'https://metrics.operas-eu.org/crossref/citations/v1',
    '2024-07-01 00:00:00', 230, 'event_uri_random_7', 'urn:iso:std:3166:-2:ZM',
    'user7@example.com'),
(gen_random_uuid(),
    'info:doi:10.11648/obp.0001',
    'https://metrics.operas-eu.org/crossref/citations/v1',
    '2024-07-05 00:00:00', 220, 'event_uri_random_8', 'urn:iso:std:3166:-2:ZM',
    'user8@example.com'),
(gen_random_uuid(),
    'info:doi:10.11649/obp.0001',
    'https://metrics.operas-eu.org/world-reader/users/v1',
    '2024-08-10 00:00:00', 260, 'event_uri_random_9', 'urn:iso:std:3166:-2:ZM',
    'user9@example.com'),
(gen_random_uuid(),
    'info:doi:10.11650/obp.0001',
    'https://metrics.operas-eu.org/world-reader/users/v1',
    '2024-10-10 00:00:00', 100, 'event_uri_random_10', 'urn:iso:std:3166:-2:ZM',
    'user10@example.com'),
(gen_random_uuid(),
    'info:doi:10.11651/obp.0001',
    'https://metrics.operas-eu.org/world-reader/users/v1',
    '2024-11-10 00:00:00', 150, 'event_uri_random_11', 'urn:iso:std:3166:-2:ZM',
    'user11@example.com');
