INSERT INTO publisher (publisher_id, publisher_name, prefix) VALUES
(gen_random_uuid(), 'University of Pennsylvania Press', 'info:doi:10.11646'),
(gen_random_uuid(), 'Columbia University Libraries', 'info:doi:10.11647'),
(gen_random_uuid(), 'Manchester University Press', 'info:doi:10.11648'),
(gen_random_uuid(), 'GSE Research Limited', 'info:doi:10.11649'),
(gen_random_uuid(), 'Old Publisher 1', 'info:doi:10.11650'),
(gen_random_uuid(), 'Old Publisher 2', 'info:doi:10.11651');
