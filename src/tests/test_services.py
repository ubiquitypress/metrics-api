import json
import unittest
from unittest.mock import patch

from api import urls
from web import application

from servicesctrl import (
    add_cached_service_code,
    add_service_works,
    get_cached_service_codes,
    get_service_works,
    remove_cached_service_code,
    remove_service_works,
    uris_are_invalid,
)

from .testapp import AuthApp


class AnyOrder:
    """Helper object that compares lists/sets/etc regardless of order."""
    def __init__(self, obj):
        self.obj = obj
        self.cls = type(obj)

    def __eq__(self, other):
        return isinstance(other, self.cls) and set(self.obj) == set(other)

    def __ne__(self, other):
        return not isinstance(other, self.cls) or set(self.obj) != set(other)

    def __repr__(self):
        return f"<ANY_ORDER {self.cls.__name__}>"


class TestService(unittest.TestCase):

    def setUp(self):
        self.app = application(urls, globals())
        self.authapp = AuthApp()

    def test_uris_are_invalid_flags_invalid_uris(self):
        invalid_uris = ['uri_1', 'uri_2', 'uri_3']
        is_invalid = uris_are_invalid(invalid_uris)
        self.assertEqual(is_invalid, True)

    def test_uris_are_invalid_ignores_valid_uris(self):
        valid_uris = [('uri_1', 'id_1'), ('uri_2', 'id_2'), ('uri_3', 'id_3')]
        is_invalid = uris_are_invalid(valid_uris)
        self.assertEqual(is_invalid, False)

    @patch('servicesctrl.get_cache_value')
    def test_get_cached_service_codes_returns_expected_values(self, get_cache):
        get_cache.return_value = ['a', 'b', 'c', 1, 2, 3]
        value = get_cached_service_codes()
        self.assertEqual(value, {'a', 'b', 'c', 1, 2, 3})

    @patch('servicesctrl.set_no_expire_cache_value')
    @patch('servicesctrl.get_cached_service_codes')
    def test_add_cached_service_code_adds_service_code_as_expected(
            self, get_services, set_cache
    ):
        get_services.return_value = {'a', 'b', 'c'}
        add_cached_service_code('test_service_code')
        set_cache.assert_called_with(
            'services/codes',
            AnyOrder(['a', 'b', 'c', 'test_service_code'])
        )

    @patch('servicesctrl.set_no_expire_cache_value')
    def test_add_work_adds_service_code(self, set_cache):
        add_service_works('test_service_code', [['a', 1], ['b', 2], ['c', 3]])
        set_cache.assert_called_with(
            'services/works/test_service_code',
            AnyOrder([('a', 1), ('b', 2), ('c', 3)])
        )

    @patch('servicesctrl.set_no_expire_cache_value')
    @patch('servicesctrl.get_cached_service_codes')
    @patch('servicesctrl.delete_cache_value')
    def test_remove_cached_service_code_removes_service_code_as_expected(
            self, delete_cache, get_services, set_cache
    ):
        get_services.return_value = {'a', 'b', 'c', 'test_service_code'}
        remove_cached_service_code('test_service_code')
        set_cache.assert_called_with(
            'services/codes',
            AnyOrder(['a', 'b', 'c'])
        )
        delete_cache.assert_called_with('services/works/test_service_code')

    @patch('servicesctrl.get_cache_value')
    def test_get_work_calls_get_from_cache(self, get_cache):
        get_cache.return_value = [('get_cache_return_value',)]
        value = get_service_works('test_service_code')
        get_cache.assert_called()
        self.assertEqual(value, {('get_cache_return_value',)})

    @patch('servicesctrl.get_cache_value')
    @patch('servicesctrl.set_no_expire_cache_value')
    def test_remove_service_works_removes_expected_works(
            self, set_cache, get_cache
    ):
        get_cache.return_value = [('test_1', 1), ('test_2', 2), ('test_3', 3)]
        remove_service_works('test_service_code', [['test_1', 1], ['test_2', 2]])
        get_cache.assert_called()
        set_cache.assert_called_with(
            'services/works/test_service_code', [('test_3', 3)]
        )
    
    @patch('servicesctrl.get_service_works')
    def test_get_service_codes_ok(self, service_function):
        service_function.return_value = [1, 2, 3]
        res = self.app.request(
            '/services?service_code=not_important',
            method='GET'
        )
        results = json.loads(res.data)
        self.assertEqual(results["status"], "ok")
        self.assertEqual(results["code"], 200)

    @patch('servicesctrl.get_service_works')
    @patch('servicesctrl.set_no_expire_cache_value')
    @patch('servicesctrl.add_cached_service_code')
    def test_post(self, add_service, set_cache, get_works):
        get_works.return_value = {('value_1', 1), ('value_2', 2)}
        data = {'service_code': 'N/A', 'uris': [('value_3', 3), ('value_4', 4)]}
        headers = self.authapp.auth_headers
        self.app.request(
            f'/services',
            method='POST',
            data=json.dumps(data),
            headers=headers
        )
        set_cache.assert_called_with(
            'services/works/N/A',
            AnyOrder(
                [
                    ('value_1', 1),
                    ('value_2', 2),
                    ('value_3', 3),
                    ('value_4', 4)
                ]
            )
        )
        add_service.assert_called_with('N/A')

    @patch('servicesctrl.get_service_works')
    @patch('servicesctrl.set_no_expire_cache_value')
    @patch('servicesctrl.remove_cached_service_code')
    def test_delete_removes_works(self, remove_service, set_cache, get_works):
        get_works.return_value = {('value_1', 1), ('value_2', 2), ('value_3', 3)}
        data = {'service_code': 'N/A', 'uris': [('value_2', 2), ('value_3', 3)]}
        headers = self.authapp.auth_headers
        self.app.request(
            f'/services',
            method='DELETE',
            data=json.dumps(data),
            headers=headers
        )
        set_cache.assert_called_with(
            'services/works/N/A',
            [('value_1', 1)]
        )
        remove_service.assert_not_called()

    @patch('servicesctrl.get_service_works')
    @patch('servicesctrl.set_no_expire_cache_value')
    @patch('servicesctrl.remove_cached_service_code')
    def test_delete_removes_srvice_if_all_works_removed(
            self, remove_service, set_cache, get_works
    ):
        get_works.return_value = {('value_1', 1), ('value_2', 2), ('value_3', 3)}
        data = {'service_code': 'N/A', 'uris': [('value_1', 1), ('value_2', 2), ('value_3', 3)]}
        headers = self.authapp.auth_headers
        self.app.request(
            f'/services',
            method='DELETE',
            data=json.dumps(data),
            headers=headers
        )
        set_cache.assert_called_with('services/works/N/A', [])
        remove_service.assert_called_with('N/A')

    @patch('servicesctrl.remove_service_works')
    @patch('servicesctrl.remove_cached_service_code')
    def test_delete_removes_service_when_no_uris_specified(
            self, remove_service, remove_works
    ):
        data = {'service_code': 'N/A'}
        headers = self.authapp.auth_headers
        self.app.request(
            f'/services',
            method='DELETE',
            data=json.dumps(data),
            headers=headers
        )
        remove_works.assert_not_called()
        remove_service.assert_called_with('N/A')
