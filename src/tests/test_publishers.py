import json
import unittest
from datetime import datetime
from unittest import mock
from unittest.mock import MagicMock, patch

import web
from web import application

from api import db, urls

from publishersctrl import fetch_work_uri_data_from_crossref, set_publishers

from models.publishers import Publisher


class TestPublishers(unittest.TestCase):

    def setUp(self):
        self.app = application(urls, globals())

        # Set up web.ctx and web.input
        web.ctx = mock.MagicMock()
        web.ctx.status = '200 OK'
        web.ctx.headers = [('Content-Type', 'application/json;charset=UTF-8')]

        # Set up any other necessary context attributes
        web.ctx.env = {
            'REQUEST_METHOD': 'GET',
            'PATH_INFO': '/publishers',
            'QUERY_STRING': 'filter=work_uri:tag:ubiquitypress.com'
        }

    def test_get_all_publishers(self):
        """Six total publishers"""
        query = f'''SELECT publisher_name, prefix
            FROM publisher
            '''

        res = self.app.request(
            f'/publishers',
            method='GET'
        )
        results = json.loads(res.data)
        self.assertEqual(results["code"], 200)
        self.assertEqual(results["status"], "ok")
        self.assertEqual(results["count"], 6)
        self.assertEqual(results["data"][0]["prefix"], "info:doi:10.11646")
        self.assertEqual(
            results["data"][0]["publisher_name"],
            "University of Pennsylvania Press"
        )

    @patch('models.publishers.Publisher.get_doi_prefixes')
    @patch('publishersctrl.fetch_work_uri_data_from_crossref')
    def test_set_publishers(self, mock_fetch_data, mock_get_doi_prefixes):
        # Mock data
        mock_get_doi_prefixes.return_value = [
            {'work_uri':'info:doi:10.1234/123', 'truncated_work_uri': '10.1234'}
        ]
        mock_fetch_data.side_effect = [{'name': 'Test Publisher', 'prefix': 'prefix/10.1234'}]

        last_day_month = datetime(2024, 2, 29)
        set_publishers(last_day_month)

        mock_get_doi_prefixes.assert_called_once_with(last_day_month)
        mock_fetch_data.assert_called_once_with(
            {'work_uri': 'info:doi:10.1234/123', 'truncated_work_uri': '10.1234'}
        )

    @patch('publishersctrl.requests.get')
    def test_fetch_work_uri_data_from_crossref(self, mock_requests_get):
        """Test the crossref method."""
        mock_response = MagicMock()
        mock_response.json.return_value = {'message': {
            'name': 'Test Publisher',
            'prefix': 'https://id.crossref.org/prefix/10.1234'}
        }
        mock_response.status_code = 200
        mock_requests_get.return_value = mock_response

        prefix = {'truncated_work_uri': '10.1234'}
        result = fetch_work_uri_data_from_crossref(prefix)

        mock_requests_get.assert_called_once_with('https://api.crossref.org/prefixes/10.1234')
        self.assertEqual(result, {'publisher': 'Test Publisher', 'prefix': 'info:doi:10.1234'})

    @patch('models.publishers.do_query')  # Patch where `do_query` is actually used
    def test_get_doi_prefixes(self, mock_do_query):
        """Test if get_doi_prefixes fetches correct prefixes."""

        # Mock return value as list of dicts (or web.py Storage objects if needed)
        mock_data = [
            {'work_uri': 'info:doi:10.11647/obp.0002', 'truncated_work_uri': 'info:doi:10.11647'},
            {'work_uri': 'info:doi:10.11648/obp.0001', 'truncated_work_uri': 'info:doi:10.11648'},
            {'work_uri': 'info:doi:10.11649/obp.0001', 'truncated_work_uri': 'info:doi:10.11649'},
            {'work_uri': 'info:doi:10.11650/obp.0001', 'truncated_work_uri': 'info:doi:10.11650'},
            {'work_uri': 'info:doi:10.11651/obp.0001', 'truncated_work_uri': 'info:doi:10.11651'}
        ]
        mock_do_query.return_value = mock_data

        test_date = datetime(2024, 12, 20)

        result = Publisher.get_doi_prefixes(test_date)

        mock_do_query.assert_called_once()  # Ensure function was called once
        self.assertEqual(result, mock_data)  # Check if returned data matches mock
