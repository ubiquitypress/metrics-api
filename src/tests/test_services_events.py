import json
import unittest
from unittest.mock import patch, MagicMock
from service_eventsctrl import ServiceEventsController
import web

# Mock Redis Cache
MOCK_SERVICE_METRICS = ['metric1', 'metric2']


class TestServiceEventsController(unittest.TestCase):
    def setUp(self):
        """Set up the mocked web.ctx."""
        # Mock web.ctx to simulate the web.py request context
        web.ctx = MagicMock()
        web.ctx.headers = []
        web.ctx.env = {
            "HTTP_TRANSFER_ENCODING": "chunked",
            "REQUEST_METHOD": "GET"
        }

        web.input = MagicMock(
            return_value={'start_date': '2024-03-01', 'service_code': 'test-service'}
        )

        web.header = MagicMock()

    @patch('service_eventsctrl.get_cache_value')
    @patch('service_eventsctrl.set_cache_value')
    @patch('service_eventsctrl.get_metrics_service_timestamp')
    def test_get_endpoint_with_cache(
            self, mock_get_metrics, mock_set_cache, mock_get_cache
    ):
        """
        Test GET endpoint when cache has metrics.
        """
        # Arrange
        mock_get_cache.return_value = MOCK_SERVICE_METRICS

        controller = ServiceEventsController()

        # Act
        response = controller.GET()

        # Assert
        self.assertEqual(
            response,
            f'{{"status": "ok", "code": 200, "count": 2, "data": {json.dumps(MOCK_SERVICE_METRICS)}}}'
        )
        mock_get_cache.assert_called_once_with('services/metrics/test-service')
        mock_get_metrics.assert_not_called()
        mock_set_cache.assert_not_called()

    @patch('service_eventsctrl.get_cache_value')
    @patch('service_eventsctrl.set_cache_value')
    @patch('service_eventsctrl.get_metrics_service_timestamp')
    def test_get_endpoint_without_cache(
            self, mock_get_metrics, mock_set_cache, mock_get_cache
    ):
        """
        Test GET endpoint when cache is empty, and metrics are fetched.
        """
        # Arrange
        start_date = '2024-03-01'
        service_code = 'test-service'
        web.input.return_value = {'start_date': start_date, 'service_code': service_code}
        mock_get_cache.return_value = None  # Simulate empty cache
        mock_get_metrics.return_value = MOCK_SERVICE_METRICS

        controller = ServiceEventsController()

        # Act
        response = controller.GET()

        # Assert
        self.assertEqual(
            response,
            f'{{"status": "ok", "code": 200, "count": 2, "data": {json.dumps(MOCK_SERVICE_METRICS)}}}'
        )
        mock_get_cache.assert_called_once_with('services/metrics/test-service')
        mock_get_metrics.assert_called_once_with(start_date, service_code)
        mock_set_cache.assert_called_once_with('services/metrics/test-service', MOCK_SERVICE_METRICS)


if __name__ == '__main__':
    unittest.main()
