import json
import unittest
from unittest.mock import patch, MagicMock
import web

from citationsctrl import CitationsController  # Adjust the import path as necessary

# Mock data for tests
MOCK_CITATION_DATA = [{'title': 'Mock Title', 'author': 'Mock Author'}]

class TestCitationsController(unittest.TestCase):
    def setUp(self):
        """Set up the mocked web.ctx."""
        # Mock web.ctx to simulate the web.py request context
        web.ctx = MagicMock()
        web.ctx.headers = []
        web.ctx.env = {
            "HTTP_TRANSFER_ENCODING": "chunked",
            "REQUEST_METHOD": "GET"  # Simulate a GET request
        }

        web.input = MagicMock(return_value={'work_uri': '10.1234/mock-doi'})

        web.header = MagicMock()

    @patch('citationsctrl.get_cache_value')
    @patch('citationsctrl.set_cache_value')
    @patch('models.citation.DoiMetadata.get_citation_metadata')
    def test_get_citations_with_cache(
            self, get_citation_metadata, mock_set_cache, mock_get_cache
    ):
        """
        Test GET endpoint when cache is empty, and metadata is fetched.
        """
        mock_get_cache.return_value = MOCK_CITATION_DATA

        controller = CitationsController()

        # Act and Assert
        try:
            response = controller.GET('test')
            self.assertEqual(
                response,
                f'{{"status": "ok", "code": 200, "count": 1, "data": {json.dumps(MOCK_CITATION_DATA)}}}'
            )
        except web.HTTPError as e:
            self.fail(f"GET raised HTTPError unexpectedly: {e}")

        # Assert
        mock_get_cache.assert_called_once_with('citations/10.1234/mock-doi')
        get_citation_metadata.assert_not_called()
        mock_set_cache.assert_not_called()

    @patch('citationsctrl.get_cache_value')
    @patch('citationsctrl.set_cache_value')
    @patch('doi_metadata_logic.filter_metadata')
    @patch('models.citation.DoiMetadata.get_citation_metadata')
    def test_get_citations_without_cache(
            self,
            get_citation_metadata,
            filter_metadata,
            mock_set_cache,
            mock_get_cache
    ):
        """
        Test GET endpoint when cache is empty, and metadata is fetched.
        """
        # Arrange
        mock_get_cache.return_value = None  # Simulate empty cache
        web.input.return_value = {
            'work_uri': 'tag:ubiquitypress.com'
        }

        mock_get_citation_data = [
            {
            'doi': 'event_uri_random_2',
            'metadata':
                {
                    'doi': 'event_uri_random_2',
                    'page': '1-10',
                    'type': 'journal-article',
                    'year': 2024,
                    'issue': 'Dummy Issue',
                    'title': 'Dummy Title',
                    'source': 'Dummy Source',
                    'volume': 'Dummy Volume',
                    'authors': [{'name': 'John Doe'}],
                    'editors': [{'name': 'Jane Smith'}],
                    'published': {
                        'date-parts': [[2024, 1, 1]]
                    }
                }
        }]

        expected_data = [
            {
                "authors": [],
                "editors": [],
                "year": 2024,
                "title": "D",
                "source": None,
                "volume": "Dummy Volume",
                "issue": "Dummy Issue",
                "page": "1-10",
                "doi": None,
                "type": "journal-article"
            }
        ]
        get_citation_metadata.return_value = mock_get_citation_data

        expected_result = (
            f'{{"status": "ok", "code": 200, "count": 1, "data": {json.dumps(expected_data)}}}'
        )

        controller = CitationsController()

        # Act and Assert
        try:
            response = controller.GET('test')
            self.assertEqual(
                response,
                expected_result
            )
        except web.HTTPError as e:
            self.fail(f"GET raised HTTPError unexpectedly: {e}")

        # Assert
        mock_get_cache.assert_called_once_with('citations/tag:ubiquitypress.com')
        mock_set_cache.assert_called_once_with('citations/tag:ubiquitypress.com', expected_data)

if __name__ == '__main__':
    unittest.main()
