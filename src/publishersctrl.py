from datetime import datetime
from typing import Optional, Dict, Any

import requests
import web

from api import json_response, api_response
from aux import debug_mode, logger_instance

from cache import get_cache_value, set_cache_value
from models.publishers import Publisher


logger = logger_instance(__name__)
web.config.debug = debug_mode()


class PublishersController:
    """Handles actions related to publishers metrics"""

    @json_response
    @api_response
    def GET(self, *args):
        """Get list of publishers.
        """

        redis_key = 'publishers'
        data = get_cache_value(redis_key)

        if data is None:
            data = Publisher.get_publishers()

            set_cache_value(redis_key, data)
        return data

    @json_response
    def OPTIONS(self):
        return


def set_publishers(last_day_month: datetime) -> None:
    """Run monthly to set the publishers in the database.
    All entries will have the last day of the month as timestamp."""

    uris = Publisher.get_doi_prefixes(last_day_month)
    api_results = list(map(fetch_work_uri_data_from_crossref, uris))

    for entry in api_results:
        if not entry:
            logger.warning(f"No data returned for some DOI prefixes: {uris}")
            continue

        publisher_name = entry.get('publisher')
        prefix = entry.get('prefix')

        if not publisher_name or not prefix:
            logger.warning(f"Missing data in: {entry}")
            continue

        publisher = Publisher(
            publisher_name,
            prefix
        )

        try:
            publisher.save()
            logger.info(f"Saved publisher: {publisher.publisher_name}")
        except Exception as e:
            logger.error(
                f"Failed to save publisher: {publisher.__dict__} | Error: {e}"
            )


def fetch_work_uri_data_from_crossref(
        prefix: dict
) -> Optional[Dict[str, Any]]:
    """Call crossref endpoint to translate the DOIs
    to the publishers names. If not found will try
    with DataCite instead.
    """
    prefix = prefix['truncated_work_uri']
    url = f"https://api.crossref.org/prefixes/{prefix}"
    response = requests.get(url)

    if response is None or response.status_code == 404:
        return fetch_work_uri_data_from_datacite(prefix)
    data = response.json()['message']
    return {
        'publisher': data.get('name'),
        'prefix': data.get('prefix').replace(
            "https://id.crossref.org/prefix/", "info:doi:"
        )
    }


def fetch_work_uri_data_from_datacite(
        doi_prefix: str
) -> Optional[Dict[str, Any]]:
    """
    Call the DataCite REST API to retrieve publishers. Log 404 errors to a set.

    Args:
        doi_prefix (str): The work URI (prefix).

    Returns:
        Optional[Dict[str, Any]]: Data from the API, or None if not found
        or an error occurred.
    """

    doi_prefix_clean = doi_prefix.replace('info:doi:', '')

    url = f'https://api.datacite.org/dois?prefix={doi_prefix_clean}'

    try:
        response = requests.get(url)
        response_data = response.json()

        if response.status_code == 200 and response_data.get('data'):
            prefix = response_data['meta']['prefixes'][0]['id']
            return {
                'publisher': response_data['data'][0]['attributes'].get(
                    'publisher'
                ),
                'prefix': f'info:doi:{prefix}'
            }

    except requests.exceptions.RequestException as e:
        logger.error(
            f'Error fetching {doi_prefix}: {e}'
        )
