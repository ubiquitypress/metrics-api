import json
from typing import List

from .queries import do_query
from api import db


class DoiMetadata:
    def __init__(self, doi, metadata):
        self.doi = doi
        self.metadata = metadata

    def save(self):
        """Save the doi_metadata record to the database."""
        q = '''
            INSERT INTO doi_metadata (doi, metadata)
            VALUES ($doi, $metadata::jsonb)
            ON CONFLICT (doi) DO NOTHING;
        '''
        options = {
            'doi': self.doi,
            'metadata': json.dumps(self.metadata)
        }
        do_query(q, options)

    def to_dict(self):
        return {
            'doi': self.doi,
            'metadata': self.metadata,
        }

    @staticmethod
    def get_distinct_dois_without_metadata() -> List:
        """Get DOIs or event_uris from citation events that do not
        have associated entries in the doi_metadata table."""
        cit_measure = 'https://metrics.operas-eu.org/crossref/citations/v1'
        query = f'''
            SELECT DISTINCT event_uri FROM event
                WHERE measure_uri='{cit_measure}'
            EXCEPT
                SELECT doi FROM doi_metadata;
        '''

        results = db.query(query)
        return [row.event_uri for row in results]

    @staticmethod
    def get_citation_metadata(work_uri) -> List:
        """Retrieve citation metadata for a specific work URI.
        NOTE: A work_uri can have multiple event_uris,
        thus multiple dois could be returned."""
        cit_measure = 'https://metrics.operas-eu.org/crossref/citations/v1'
        query = f'''
            SELECT *
            FROM doi_metadata
            WHERE doi IN (
                SELECT DISTINCT event_uri
                FROM event
                WHERE work_uri='{work_uri}'
                AND measure_uri='{cit_measure}'
            )
        '''

        results = db.query(query)

        return list(results)
