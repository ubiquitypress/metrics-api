from datetime import datetime
import uuid

from api import db
from .queries import do_query


class Publisher:
    def __init__(self, publisher_name, prefix):
        self.publisher_name = publisher_name
        self.prefix = prefix

    @staticmethod
    def get_publishers():
        q = '''SELECT publisher_name, prefix
            FROM publisher
        '''
        results = db.query(q)

        return list(results)

    @staticmethod
    def get_doi_prefixes(date: datetime) -> list:
        """
        Query to extract the prefixes from the Event table based on a date,
        returning only those prefixes not found in the publishers table.
        """
        formatted_date = date.strftime('%Y-%m-%d %H:%M:%S')

        query = f'''
            SELECT DISTINCT ON (truncated_work_uri)
              work_uri,
              SUBSTRING(work_uri FROM '^([^/]+)') AS truncated_work_uri
            FROM
              event
            WHERE
              work_uri LIKE 'info:doi:10%'
              AND timestamp < '{formatted_date}'
            AND SUBSTRING(work_uri FROM '^([^/]+)') NOT IN (
                SELECT prefix FROM publisher
            );
        '''
        return list(do_query(query))

    def save(self):
        new_uuid = uuid.uuid4()
        q = '''INSERT INTO publisher (publisher_id, publisher_name, prefix)
               VALUES ($publisher_id, $publisher_name, $prefix)
               ON CONFLICT DO NOTHING;'''

        params = {
            "publisher_id": str(new_uuid),
            "publisher_name": self.publisher_name,
            "prefix": self.prefix
        }

        do_query(q, params)
