from collections import defaultdict
from datetime import timedelta, date
from typing import Dict, List, Set, Tuple

from api import db

from cache import set_no_expire_cache_value


class Popularity:
    """Get popularity from the event table and measure joined."""
    @staticmethod
    def get_metrics_by_type(uris, start_date, end_date):
        query = f'''SELECT
           e.work_uri,
           m.type,
           SUM(e.value) AS total
        FROM event e
        JOIN measure m ON e.measure_uri = m.measure_uri
        WHERE
           e.work_uri IN {uris}
           AND m.type IN (
              'citations',
              'downloads',
              'reads',
              'sessions',
              'views'
           )
           AND e.timestamp BETWEEN '{start_date}'::timestamp
           AND '{end_date}'::timestamp
        GROUP BY e.work_uri, m.type;'''

        results = db.query(query)
        return list(results)


def set_popularity_metrics(
        uri_to_id_mapping: Set[Tuple],
        weights: Dict,
        service_code: str
) -> None:
    """
    Run daily to set the popularity metrics values in the cache

    uri_to_id_mapping: {
        (work_uri_1, id_1),
        (work_uri_2, id_2),
        <etc>
    }
    weights: "books": {
            "timeframe": 180,
            "weightings": {
                "citations": 20,
                "downloads": 30,
                "reads": 40,
                "views": 10
            }
        }
    """
    weights_extract = get_books_or_journal_weights(service_code, weights)
    today = date.today()
    timeframe = weights_extract.get('timeframe', 365)
    start_date = (today - timedelta(days=timeframe)).isoformat()
    end_date = today.isoformat()
    work_uris = tuple(item[0] for item in uri_to_id_mapping)
    metrics = Popularity.get_metrics_by_type(work_uris, start_date, end_date)
    grouped_data = group_metrics_by_work_uri(metrics)
    scored_works = calculate_the_weighted_score(
        grouped_data, weights_extract['weightings'], uri_to_id_mapping
    )
    top_10_weighted_ids = get_top_10_external_ids(scored_works)

    redis_key_base = f'popularity/{service_code}'
    set_no_expire_cache_value(f'{redis_key_base}/metrics', top_10_weighted_ids)
    set_no_expire_cache_value(f'{redis_key_base}/last_checked', end_date)


def get_books_or_journal_weights(service_code: str, weights: Dict) -> Dict:
    """Get journal weights or books depending on service code.
    It defaults all values to 1 and timeframe to 365 days if not found.
    """
    if not weights:
        return {
            'weightings': {
                'citations': 1,
                'downloads': 1,
                'reads': 1,
                'views': 1
            },
            'timeframe': 365
        }

    elif '-j-' in service_code:
        return weights.get('journals')
    elif '-p-' in service_code:
        return weights.get('books')


def group_metrics_by_work_uri(works_data: List) -> Dict:
    """Group the data by work_uri
    works_data: [
        {'work_uri': 'info:doi:10...', 'type': 'citations', 'total': 100}
        ...
    ]
    """
    grouped_data = defaultdict(lambda: defaultdict(int))
    for item in works_data:
        grouped_data[item['work_uri']][item['type']] = item['total']

    return grouped_data


def calculate_the_weighted_score(
        grouped_data: defaultdict,
        weightings: Dict,
        uri_to_id_mapping: Set[Tuple]
) -> List[Dict]:
    """Multiply by each weight and sum them all for each work_uri's metric
    type (downloads, reads, citations...)
    """
    uri_to_id_dict = dict(uri_to_id_mapping)

    scored_works = []
    for work_uri, metrics in grouped_data.items():
        # Views and sessions are the same thus add them
        views = metrics.get('views', 0) + metrics.get('sessions', 0)
        metrics.update(views=views)

        score = sum(
            metrics.get(metric_type, 0) * weightings.get(metric_type, 0)
            for metric_type in weightings
        )
        scored_works.append({
            'work_uri': work_uri,
            'score': score,
            'external_id': uri_to_id_dict[work_uri]
        })

    return scored_works


def get_top_10_external_ids(scored_works: List[Dict]) -> List:
    """
    Sort the works by score in descending order and
    Get the external IDs of the top 10 works
    scored_works: [
        {'work_uri': 'abc', 'score': 2, 'external_id': '123'},
        {'work_uri': 'def', 'score': 30, 'external_id': '456'}
    ]
    """
    sorted_works = sorted(scored_works, key=lambda x: x['score'], reverse=True)
    top_10_external_ids = [work['external_id'] for work in sorted_works[:10]]

    return top_10_external_ids
