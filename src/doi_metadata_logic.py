import logging
from typing import Dict, List

import requests

from models.citation import DoiMetadata


def fetch_doi_metadata_from_crossref(doi):
    """Call the Crossref API to fetch metadata for a doi"""

    url = (
        f'https://api.crossref.org/v1/works/{doi}'
        '?mailto=tech@ubiquitypress.com'
    )

    response = requests.get(url)
    if response.status_code == 200:
        data = response.json()
        return data['message']
    else:
        logging.error(f"Error fetching data for the doi: {doi}")


def save_new_citation_event_metadata(doi: str) -> Dict:
    """Fetches new dois or event_uris related to crossref citations
    corresponding metadata, and saves to the database."""

    metadata = fetch_doi_metadata_from_crossref(doi)
    if metadata:
        doi_metadata = DoiMetadata(
            doi=doi,
            metadata=metadata,
        )
        doi_metadata.save()
    else:
        logging.error(
            f'[DOI metadata]{doi} No metadata has been fetched.'
        )
    return doi_metadata.to_dict()


def filter_metadata(doi_metadata: Dict) -> Dict:
    """Filter the metadata to return the required fields
    for the frontend to be displayed."""

    result = {}
    try:
        metadata = doi_metadata['metadata']

        # Extract authors and editors
        result['authors'] = handle_authors_and_editors(
            metadata.get('author', [])
        )
        result['editors'] = handle_authors_and_editors(
            metadata.get('editor', [])
        )

        result['year'] = metadata.get(
            'published', {}
        ).get('date-parts')[0][0]

        result['title'] = metadata.get('title', [None])[0]

        if metadata.get('container-title') and metadata.get(
            'type'
        ) == 'journal-article':
            result['source'] = metadata.get('container-title')[0]
        else:
            result['source'] = metadata.get('publisher', None)

        volume_issue = metadata.get('volume')
        result['volume'] = volume_issue
        issue = metadata.get('issue', None)
        result['issue'] = issue if volume_issue and issue else None

        result['page'] = metadata.get('page', None)
        result['doi'] = metadata.get('DOI', None)

        # Edited-book, journal-article, monograph or book-chapter
        result['type'] = metadata.get('type', None)

    except KeyError as e:
        print(f"Key error: {e}")
        result['error'] = f"Missing key in metadata: {e}"

    return result


def handle_authors_and_editors(
        metadata: List[Dict]
) -> List[Dict]:
    """Extract the last_name and initial from author
    or editors"""
    result = []
    for item in metadata:
        last_name = item.get('family')
        initial = item.get('given', '')[0] + '.' if item.get('given') else ''
        if last_name and initial:
            result.append({'last_name': last_name, 'initial': initial})
    return result
