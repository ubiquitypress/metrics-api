"""Contains classes, logic, etc. for tracking groups of works that belong to a
service (jornal, press, etc...)
"""
from datetime import datetime
from typing import List

import web

from api import json_response, api_response
from aux import logger_instance, debug_mode
from cache import get_cache_value, set_cache_value
from models.event import Event
from models.aggregation import Aggregation

logger = logger_instance(__name__)
web.config.debug = debug_mode()

SERVICE_WORKS_REDIS_KEY_BASE = 'services/works'


class ServiceEventsController:
    """Handles actions related to events for all works in a service"""

    @json_response
    @api_response
    def GET(self, name=None):
        start_date = web.input().get('start_date')
        service_code = web.input().get('service_code')
        redis_key = f'services/metrics/{service_code}'
        metrics = get_cache_value(redis_key)

        if not metrics:
            metrics = get_metrics_service_timestamp(
                start_date, service_code
            )
            set_cache_value(redis_key, metrics)

        return metrics


def get_metrics_service_timestamp(
        start_time: str, service_code: str
):
    """Get list of works registered to a given service.
    use the start_time as the first date to collect metrics up to now.
    """
    # Ensure start_time is in the correct format (ISO 8601)
    if not datetime.strptime(start_time, '%Y-%m-%d'):
        logger.error(f"Wrong start_time format: {start_time}")
        raise ValueError

    redis_key = f'{SERVICE_WORKS_REDIS_KEY_BASE}/{service_code}'
    service_works = get_cache_value(redis_key) or []
    work_uris = [service[0] for service in service_works]

    return get_filtered_by_timestamp(start_time, work_uris)


def get_filtered_by_timestamp(
        start_time: str, work_uris: List
) -> List:
    """
    Get the timestamp, work_uris and return the measures aggregated.
    """
    if not work_uris:
        raise ValueError("work_uris list cannot be empty")

    clause = (
        f" AND work_uri IN {tuple(work_uris)}"
        f"  AND timestamp > '{start_time}' "
    )

    criterion = 'measure_uri'
    aggregation = Aggregation(criterion)
    aggregation.data = Event.get_for_aggregation(
        'measure_uri', clause, {}
    )
    return aggregation.aggregate()
