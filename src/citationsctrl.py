import web

from api import json_response, api_response
from aux import logger_instance, debug_mode
from cache import get_cache_value, set_cache_value
from models.citation import DoiMetadata
from doi_metadata_logic import filter_metadata


logger = logger_instance(__name__)
web.config.debug = debug_mode()

DOI_METADATA_REDIS_KEY_BASE = 'citations'


class CitationsController:
    """Handles doi_metadata citations from Crossref."""

    @json_response
    @api_response
    def GET(self, name):
        """Retrieve citation metadata for a given work URI."""
        work_uri = web.input().get('work_uri')
        redis_key = f'{DOI_METADATA_REDIS_KEY_BASE}/{work_uri}'

        citation_data = get_cache_value(redis_key)
        if not citation_data:
            citation_data = [
                filter_metadata(doi_metadata)
                for doi_metadata
                in DoiMetadata.get_citation_metadata(work_uri)
            ]

            set_cache_value(redis_key, citation_data)
        return citation_data

    @json_response
    def OPTIONS(self, name):
        return
