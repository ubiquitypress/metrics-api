from celery.app.control import Control

from tasks.tasks import celery_app


app_control = Control(celery_app)
app_inspect = app_control.inspect()

if not app_inspect.active():
    raise Exception('No active workers found.')

# It is possible for this to fail for other reasons
